# Introduction 
.NET 9 implementation of the SiLA2 Standard (https://sila-standard.com/)
  * Platform-independent
  * SiLA2.Server even runs on embedded Linux Host as recommended AspNetCore-Process
  * Feature-To-Proto-Generation by XSLT
    * SiLA2 Core Features included in SiLA2.dll
    * Additional Features should be part of Feature-Implementation Assemblies (just add your features and protos in MSBuild Targets ProtoPreparation & ProtoGeneration like it is done in Assemblies TemperatureController.Features.csproj or ShakerController.Features.csproj)
  * Extensible InProcess Server Web Frontend included (based on Blazor (https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor) which supports "server push" functionality by SignalR) 

   ![image](https://gitlab.com/SiLA2/sila_csharp/-/raw/master/misc/web_frontend_realtime_temperature_monitoring.gif) 

  * Optional InProcess-Database-Module (based on SQLite) with basic User Management which can be easily extended.
  * Optional InProcess-Document-Database-Module (based on LiteDB) named SiLA2.Database.NoSQL which can be used as AnIML data storage...
  * Optional Inter Process Communication Module (SiLA2.IPC.NetMQ) for Socket Communication...compatible with ZeroMQ (https://zeromq.org/)
  * Optional AnIML Module offering the AnIML Domain Model as C# classes generated from official AnIML schemas (https://github.com/AnIML/schemas)
  * Optional Component to manage Encryption and Certifcates

  There´s also a Web Application [SiLA2.UniversalClient.Net](https://gitlab.com/SiLA2/sila_csharp/-/tree/master/src/SiLA2.UniversalClient.Net) to control your running SiLA2.Servers in your local network.

  If you want to see all the Modules and Components you should start SiLA2.Temperature.Server.App and afterwards SiLA2.Temperature.Server.App.Webfrontend.
  Running the Server the first time, you´ll have to add an exception to your browser once due to the self-signed certificate which was created by the Server if there´s none.
   
# Prerequisites
  * Linux / macOS
    * You´ll need the .NET 9 SDK >> https://dotnet.microsoft.com/download/dotnet/9.0
	* It´s not necessary to build applications with a GUI but if you do so an IDE like Visual Studio Code ( >> https://code.visualstudio.com/ ) would be convenient especially with Extension C# Dev Kit ( >> https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csdevkit ) 
  * Windows
    * Download free IDE Visual Studio 2022 Community ( >> https://visualstudio.microsoft.com/de/vs/community/ ), use commercial Visual Studio 2022 Version or Visual Studio Code as well
	* .NET 9 SDK is included in Visual Studio 2022 (>= Version 17.12.x)...if you want to use Visual Studio Code or other IDEs you´ll have to download it on your own (see link above)

# Getting Started
  * Clone Repo
    * Please be sure fetching sila_base submodule by
      * checking out the Repository with git-submodules recursively 
        * git clone --recurse-submodules https://gitlab.com/SiLA2/sila_csharp.git
      * or check out the Repository and run following commands
        * git submodule init
        * git submodule update
  * Run gRPC-Server
    * SiLA2.Temperature.Server.Basic.App
    or
    * SiLA2.Temperature.Server.App
  * Run SiLA2.Temperature.Client.App connecting automatically to SilaServer
  or
  * Run SiLA2.Temperature.Server.App.Webfrontend (in Debug-Mode https://localhost:5011) to open a SilaServer-WebFrontend
    * In the SilaServer-WebFrontend you´ll find NavigationLink "User Management"-View to use SilaServer-Database. There´s also an example of how Server-Push-Feature can be used...just click on NavigationLink "Temperature" and hit button "Change Temperature"...the values you produced can be saved as AnIML data by pushing the according button...

# Build your own Project based on official Nuget-Packages
  * Created ASP.NET Core Application as SiLA2.Server Project
  * Search for & reference SiLA2.* packages found at Nuget.org in Visual Studio or https://www.nuget.org/ ...use at least SiLA2.Core...
  * Create *.sila.xml-Feature-File and include it like it was done in Example Project SiLA2.Referencing.Nuget.Features.csproj
  * Add MSBuild Targets ProtoPreparation & ProtoGeneration in your FeatureAssembly.csproj like
```xml
  <Target Name="ProtoPreparation" BeforeTargets="PrepareForBuild">
    <Message Text="Copying Base Protos..." Importance="high"></Message>
    <Copy SourceFiles="@(SilaProtoDefaultFiles)" DestinationFolder="Protos\" />
    <Copy SourceFiles="@(AdditionalSilaFeatureFiles)" DestinationFolder="Features\" />
    <Message Text="Started XmlTransformation TemperatureController-v1_0.sila.xml -&gt; TemperatureController.proto" Importance="high"></Message>
    <XslTransformation XslInputPath="..\..\..\sila_base\xslt\fdl2proto.xsl" 
                       XmlInputPaths="..\..\..\sila_base\feature_definitions\org\silastandard\examples\TemperatureController-v1_0.sila.xml" 
                       OutputPaths="Protos\TemperatureController.proto" />
    <Message Text="Finished XmlTransformation TemperatureController-v1_0.sila.xml -&gt; TemperatureController.proto" Importance="high"></Message>
  </Target>
``` 
```xml
  <Target Name="ProtoGeneration" DependsOnTargets="ProtoPreparation" AfterTargets="ProtoPreparation">
      <Message Text="Compiling Protos..." Importance="high"></Message>
      <ItemGroup>
        <Protobuf Include="Protos\TemperatureController.proto"
                  ProtoRoot="Protos\"
                  GrpcServices="Both"
                  OutputDir="Services\" />
        <Protobuf Update="Protos\SiLAFramework.proto"
                  ProtoRoot="Protos\"
                  CompileOutputs="false" />
      </ItemGroup>
    <Message Text="Finished Compiling Protos..." Importance="high"></Message>
  </Target>
```
  * Build Feature-Assembly
  * Implement the features you´ve defined in your *.sila.xml-Feature-File in your Feature-Assembly
  * Reference your Feature-Assenbly in your SiLA2.Server Project
  * Create SilA2-Clients communicating with the SiLA2.Server...in this case you might want to use Nuget-Package SiLA2.Client or SiLA2.Client.Dynamic

# Build and Test
* Just build your Solution as described in "# Getting Started"
* Run your SilA2.Server.App
* Call Server from a Client, use the SiLA Universal Client (https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client or use SiLA2.UniversalClient.Net ( >> https://gitlab.com/SiLA2/sila_csharp/-/tree/master/src/SiLA2.UniversalClient.Net )
* If you have problems building the solution you might clean your Nuget-Cache by 'rmdir -r %UserProfile%\.nuget\packages\*' once...

# Contribute
It´s Open Source (License >> MIT)...feel free to use or contribute...

# For Open Questions
Visit https://gitlab.com/SiLA2/sila_csharp/-/issues

 
