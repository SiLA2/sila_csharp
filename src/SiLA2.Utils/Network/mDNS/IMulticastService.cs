﻿using SiLA2.Utils.Network.Dns;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Utils.Network.mDNS
{
    public interface IMulticastService
    {
        bool IgnoreDuplicateMessages { get; set; }
        TimeSpan NetworkInterfaceDiscoveryInterval { get; set; }
        bool UseIpv4 { get; set; }
        bool UseIpv6 { get; set; }

        event EventHandler<MessageEventArgs> AnswerReceived;
        event EventHandler<byte[]> MalformedMessage;
        event EventHandler<NetworkInterfaceEventArgs> NetworkInterfaceDiscovered;
        event EventHandler<MessageEventArgs> QueryReceived;

        void Dispose();
        void OnDnsMessage(object sender, UdpReceiveResult result);
        Task<Message> ResolveAsync(Message request, CancellationToken cancel = default);
        void SendAnswer(Message answer, bool checkDuplicate = true);
        void SendAnswer(Message answer, MessageEventArgs query, bool checkDuplicate = true);
        void SendQuery(DomainName name, DnsClass klass = DnsClass.IN, DnsType type = DnsType.ANY);
        void SendQuery(Message msg);
        void SendUnicastQuery(DomainName name, DnsClass klass = DnsClass.IN, DnsType type = DnsType.ANY);
        void Start();
        void Stop();
    }
}