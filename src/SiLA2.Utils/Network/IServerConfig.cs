﻿using System;

namespace SiLA2.Utils.Network
{
    public interface IServerConfig
    {
        string DiscoveryServiceName { get; set; }
        string FQHN { get; set; }
        string Name { get; set; }
        string NetworkInterface { get; set; }
        int Port { get; set; }
        Guid Uuid { get; set; }
    }
}