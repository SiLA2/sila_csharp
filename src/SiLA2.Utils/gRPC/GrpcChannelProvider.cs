﻿using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Utils.gRPC
{
    public class GrpcChannelProvider : IGrpcChannelProvider
    {
        private readonly ILogger<GrpcChannelProvider> _logger;
        private X509Certificate2 _ca;

        private ConcurrentDictionary<string, Lazy<Task<GrpcChannel>>> _channelMap = new ConcurrentDictionary<string, Lazy<Task<GrpcChannel>>>();

        public GrpcChannelProvider(ILogger<GrpcChannelProvider> logger)
        {
            _logger = logger;
        }

        public async Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = false, X509Certificate2 ca = null, GrpcChannelOptions channelOptions = null)
        {
            try
            {
                _ca = ca;
                var key = $"https://{host}:{port}";

                var lazyChannel = _channelMap.GetOrAdd(key, _ => new Lazy<Task<GrpcChannel>>(() =>
                {
                    var httpClientHandler = new HttpClientHandler();
                    var httpClient = new HttpClient(httpClientHandler) { Timeout = Timeout.InfiniteTimeSpan };

                    if (acceptAnyServerCertificate)
                    {
                        httpClientHandler.ServerCertificateCustomValidationCallback = (httpRequestMessage, x509Certificate2, x509Chain, sSLPolicyErrors) => true;
                    }
                    else
                    {
                        httpClientHandler.ServerCertificateCustomValidationCallback = ValidateServerCertificate;
                    }

                    if (channelOptions == null)
                    {
                        channelOptions = new GrpcChannelOptions { HttpClient = httpClient };
                    }
                    else
                    {
                        if (channelOptions.HttpClient == null)
                        {
                            channelOptions.HttpClient = httpClient;
                        }
                    }
                    channelOptions.DisposeHttpClient = true;

                    return Task.FromResult(GrpcChannel.ForAddress(key, channelOptions));
                }));

                return await lazyChannel.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Failed getting Channel for Host '{host}:{port}'");
                throw;
            }
        }

        /// <summary>
        /// Validates the SSL server certificate.
        /// </summary>
        /// <param name="sender">An object that contains state information for this
        /// validation.</param>
        /// <param name="cert">The certificate used to authenticate the remote party.</param>
        /// <param name="chain">The chain of certificate authorities associated with the
        /// remote certificate.</param>
        /// <param name="sslPolicyErrors">One or more errors associated with the remote
        /// certificate.</param>
        /// <returns>Returns a boolean value that determines whether the specified
        /// certificate is accepted for authentication; true to accept or false to
        /// reject.</returns>
        private bool ValidateServerCertificate(
                object sender,
                X509Certificate2 cert,
                X509Chain chain,
                SslPolicyErrors sslPolicyErrors)
        {
            if (cert == null || chain == null)
            {
                _logger.LogError("Certificate or Certificate Chain unknown !");
                return false;
            }

            if ((sslPolicyErrors & ~SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                _logger.LogDebug($"Certificate Policy Warning {sslPolicyErrors}");
            }

            foreach (X509ChainElement element in chain.ChainElements)
            {
                chain.ChainPolicy.ExtraStore.Add(element.Certificate);
            }

            X509Certificate2Collection roots = new X509Certificate2Collection(_ca);

            chain.ChainPolicy.CustomTrustStore.Clear();
            chain.ChainPolicy.TrustMode = X509ChainTrustMode.CustomRootTrust;
            chain.ChainPolicy.CustomTrustStore.AddRange(roots);
            var result = chain.Build(cert);
            return result;
        }
    }
}
