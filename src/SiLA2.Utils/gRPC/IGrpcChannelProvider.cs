﻿using Grpc.Net.Client;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SiLA2.Utils.gRPC
{
    public interface IGrpcChannelProvider
    {
        Task<GrpcChannel> GetChannel(string host, int port, bool accceptAnyServerCertificate = false, X509Certificate2 ca = null, GrpcChannelOptions channelOptions = null);
    }
}