﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.IO.Compression;

namespace SiLA2.Utils.Extensions
{
    public static class ZipExtensions
    {
        public static Task<byte[]> GetZipFilesBytes(this IEnumerable<FileInfo> files)
        {
            using (var ms = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    foreach (var file in files)
                    {
                        var entry = zipArchive.CreateEntryFromFile(file.FullName, file.Name);
                    }
                }
                return Task.FromResult(ms.ToArray());
            }
        }
    }
}
