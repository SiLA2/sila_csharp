﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.CSharp.RuntimeBinder;

namespace SiLA2.Utils.Extensions
{
    public static class DynamicExtensions
    {
        public static object GetDynamicMember(this object obj, string memberName)
        {
            var binder = Binder.GetMember(CSharpBinderFlags.None, memberName, obj.GetType(), [CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null)]);
            var callsite = CallSite<Func<CallSite, object, object>>.Create(binder);
            return callsite.Target(callsite, obj);
        }
    }
}
