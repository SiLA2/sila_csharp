﻿namespace SiLA2.Utils.Security
{
    public class Constants
    {
        public const string SERVER_CRT_FILENAME = "server.crt";
        public const string SERVER_KEY_FILENAME = "server.key";
        public const string CA_CRT_FILENAME = "ca.crt";
        public const string CA_KEY_FILENAME = "ca.key";
    }
}
