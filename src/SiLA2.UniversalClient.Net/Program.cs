using Microsoft.AspNetCore.Server.Kestrel.Core;

using Serilog;

using SiLA2.AspNetCore;
using SiLA2.Communication.Services;
using SiLA2.Frontend.Razor.Services;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.UniversalClient.Net.Services;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;

namespace SiLA2.UniversalClient.Net
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddBlazorBootstrap();
            builder.Services.AddTransient<INetworkService, NetworkService>();
            builder.Services.AddSingleton<IServiceFinder, ServiceFinder>();
            builder.Services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            builder.Services.AddTransient<IServerDataService, ServerDataService>();
            builder.Services.AddSingleton<IConnectionViewModelProvider, ConnectionViewModelProvider>();
            builder.Services.AddTransient<IPayloadFactory, PayloadFactory>();
            builder.Services.AddTransient<IDynamicMessageService, DynamicMessageService>();
            builder.Services.AddScoped<ICommandPayloadProvider, CommandPayloadProvider>();
            builder.Services.AddSingleton<ICertificateProvider, CertificateProvider>();
            builder.Services.AddSingleton<ICertificateContext, CertificateContext>();
            builder.Services.AddSingleton<ICertificateRepository, CertificateRepository>();

            ServerConfig serverConfig = new ServerConfig(builder.Configuration["ServerConfig:Name"],
                                                         Guid.Parse(builder.Configuration["ServerConfig:UUID"]),
                                                         builder.Configuration["ServerConfig:FQHN"],
                                                         int.Parse(builder.Configuration["ServerConfig:Port"]),
                                                         builder.Configuration["ServerConfig:NetworkInterface"],
                                                         builder.Configuration["ServerConfig:DiscoveryServiceName"]);

            builder.Services.AddSingleton<IServerConfig>(serverConfig);

            builder.WebHost.ConfigureKestrel(serverOptions =>
            {
                var kestrelServerConfigData = args.GetKestrelConfigData(serverOptions.ApplicationServices);
                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                serverOptions.Listen(kestrelServerConfigData.Item1, kestrelServerConfigData.Item2, listenOptions => listenOptions.UseHttps(kestrelServerConfigData.Item3 ));
            });

            builder.Host.UseSerilog((context, configuration) =>
                configuration.ReadFrom.Configuration(context.Configuration));

#if DEBUG
            builder.AddServiceDefaults();
#endif

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}