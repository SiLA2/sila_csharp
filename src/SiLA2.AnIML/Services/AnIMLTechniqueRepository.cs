﻿using LiteDB;
using SiLA2.Database.NoSQL;

namespace SiLA2.AnIML.Services
{
    public class AnIMLTechniqueRepository : BaseRepository<AnIMLTechnique.TechniqueType>, IAnIMLTechniqueRepository
    {
        public AnIMLTechniqueRepository(ILiteDatabase db) : base(db)
        {
        }
    }
}