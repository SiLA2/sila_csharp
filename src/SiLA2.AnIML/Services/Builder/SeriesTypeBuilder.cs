﻿using AnIMLCore;

namespace SiLA2.AnIML.Services.Builder
{
    public class SeriesTypeBuilder : ISeriesTypeBuilder
    {
        public AnIMLType Build(string[] sampleSetNames)
        {
            var sampleSets = new List<SampleType>();
            foreach (var sampleSetName in sampleSetNames)
            {
                sampleSets.Add(new SampleType { name = sampleSetName, sampleID = $"SampleSetID_{Guid.NewGuid()}" });
            }
            AnIMLType anIMLType = new AnIMLType { SampleSet = new SampleSetType() { Sample = sampleSets.ToArray() }, ExperimentStepSet = new ExperimentStepSetType { id = $"ExperimentStepSetID_{Guid.NewGuid()}" } };
            anIMLType.ExperimentStepSet.ExperimentStep = [new ExperimentStepType { experimentStepID = $"ExperimentStepID_{Guid.NewGuid()}", name = "First Experiment Step" } ];
            anIMLType.ExperimentStepSet.ExperimentStep[0].Result = [new ResultType { SeriesSet = new SeriesSetType { Series = [new SeriesType { name = $"Series {Guid.NewGuid()}" }], name = $"Series Set {Guid.NewGuid()}" }, name = $"Result { Guid.NewGuid() }"  }];

            return anIMLType;
        }
    }
}
