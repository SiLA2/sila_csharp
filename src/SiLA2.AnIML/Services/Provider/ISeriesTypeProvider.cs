﻿using AnIMLCore;

namespace SiLA2.AnIML.Services.Provider
{
    public interface ISeriesTypeProvider
    {
        SeriesType GetSeriesType<T>(IEnumerable<T> items, DependencyType dependencyType = DependencyType.independent);
        SeriesType GetSeriesStringType(IEnumerable<string> items, ParameterTypeType stringType, DependencyType dependencyType = DependencyType.independent);
        SeriesType GetSeriesPNGType(IEnumerable<byte[]> items, DependencyType dependencyType = DependencyType.independent);
    }
}