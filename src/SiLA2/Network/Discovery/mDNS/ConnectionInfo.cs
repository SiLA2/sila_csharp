﻿using SiLA2.Utils.Network.Dns;
using System;
using System.Collections.Generic;

namespace SiLA2.Network.Discovery.mDNS
{
    public class ConnectionInfo : IEquatable<ConnectionInfo>
    {
        public string Address { get; }
        public int Port { get; }
        public string ServerUuid { get; }
        public string ServerName { get; }
        public string ServerVersion { get; }
        public string ServerDescription { get; }
        public string ServerType { get; }
        public string VendorUri { get; }
        public string SilaCA { get; }
        public DnsType Type { get; }

        public string ServerInfo => $"{Environment.NewLine}Server Name: {ServerName}{Environment.NewLine}Server UUID: {ServerUuid}{Environment.NewLine}Server Type: {ServerType}{Environment.NewLine}Server Version: {ServerVersion}{Environment.NewLine}Server Description: {ServerDescription}{Environment.NewLine}Server URI: {ToString()}{Environment.NewLine}Vendor URI: {VendorUri}{Environment.NewLine}";

        public ConnectionInfo(string address, int port, string serverUuid, string serverName, string serverVersion, string serverDescription, string serverType, string vendorUri, string silaCa, DnsType type)
        {
            Address = address;
            Port = port;
            ServerUuid = serverUuid;
            ServerName = serverName;
            ServerVersion = serverVersion;
            ServerDescription = serverDescription;
            ServerType = serverType;
            VendorUri = vendorUri;
            SilaCA = silaCa;
            Type = type;
        }

        public ConnectionInfo(string address, int port)
        {
            Address = address;
            Port = port;
        }

        public override string ToString()
        {
            return $"{Address}:{Port}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ConnectionInfo);
        }

        public bool Equals(ConnectionInfo other)
        {
            return other is not null &&
                   Address == other.Address &&
                   Port == other.Port &&
                   SilaCA == other.SilaCA &&
                   ServerUuid == other.ServerUuid;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Address, Port, SilaCA, ServerUuid);
        }

        public static bool operator ==(ConnectionInfo left, ConnectionInfo right)
        {
            return EqualityComparer<ConnectionInfo>.Default.Equals(left, right);
        }

        public static bool operator !=(ConnectionInfo left, ConnectionInfo right)
        {
            return !(left == right);
        }
    }
}
