﻿namespace SiLA2.Network.Discovery
{
    public class Constants
    {
        public const string SERVER_NAME = "server_name";
        public const string SERVER_TYPE = "server_type";
        public const string SERVER_VERSION = "version";
        public const string SERVER_VENDOR_URI = "vendor_url";
        public const string SERVER_DESCRIPTION = "description";
        public const int DESCRIPTION_LENGTH_LIMIT = 220;
    }
}
