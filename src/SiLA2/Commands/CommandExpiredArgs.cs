﻿using System;

namespace SiLA2.Commands
{
    /// <summary>
    /// Arguments of the event triggered upon command expiration
    /// </summary>
    public class CommandExpiredArgs : EventArgs
    {
        /// <summary>
        /// Identifier of expired command
        /// </summary>
        public Guid CommandId { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">identifier of expired command</param>
        public CommandExpiredArgs(Guid id)
        {
            CommandId = id;
        }
    }
}