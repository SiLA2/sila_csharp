﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.Commands
{
    public interface IObservableCommandManager<TParameter, TResponse>
    {
        bool IsCommandMapBusy { get; }
        Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func, TimeSpan executionDelay = new (), bool isQueued = false);
        Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func, TimeSpan executionDelay = new(), bool isQueued = false);
        void Dispose();
        ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid);
        Task RegisterForInfo(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, CancellationToken cancellationToken);
        Task ProcessIntermediateResponses<TIntermediateResponse>(CommandExecutionUUID cmdExecId, IServerStreamWriter<TIntermediateResponse> responseStream, Action<TParameter, CancellationToken> intermediateResponseHandler, CancellationToken token);
    }
}