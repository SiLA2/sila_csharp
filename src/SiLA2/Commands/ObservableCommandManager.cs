using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;

namespace SiLA2.Commands
{
    public class ObservableCommandManager<TParameter, TResponse> : IDisposable, IObservableCommandManager<TParameter, TResponse>
    {
        private readonly ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>> _commands = new ConcurrentDictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>>();
        private AutoResetEvent _commandAutoResetEvent = new AutoResetEvent(true);
        private readonly ILogger<ObservableCommandManager<TParameter, TResponse>> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private bool _disposedValue;

        public bool IsCommandMapBusy => _commands.Any(x => !x.Value.IsCompleted);

        /// <summary>
        /// The the duration during which a Command Execution UUID is valid.
        /// </summary>
        public TimeSpan LifetimeOfExecution { get; }

        public ObservableCommandManager(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            _logger = _loggerFactory.CreateLogger<ObservableCommandManager<TParameter, TResponse>>();

            LifetimeOfExecution = TimeSpan.FromSeconds(configuration.GetValue<int>("CommandLifetimeInSeconds"));

            if (LifetimeOfExecution <= TimeSpan.Zero)
            {
                throw new InvalidOperationException(
                    $"Lifetime of execution must be greater than 0 but {LifetimeOfExecution} was specified");
            }
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        /// 
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <param name="executionDelay"></param>
        /// <param name="isQueued">Queued or Parallel Processing</param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func, TimeSpan executionDelay, bool isQueued = false)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory, executionDelay, isQueued));
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        /// 
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <param name="executionDelay"></param>
        /// <param name="isQueued">Queued or Parallel Processing</param>
        /// <returns>Observable command wrapper as a task</returns>
        public async Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(TParameter parameter, Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func, TimeSpan executionDelay, bool isQueued = false)
        {
            return await AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, LifetimeOfExecution, func, _loggerFactory, executionDelay, isQueued));
        }

        /// <summary>
        /// Gets the observable command wrapper of a given command execution UUID from an Info stream
        /// </summary>
        /// <param name="commandExecutionUuid"></param>
        /// <returns></returns>
        public ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid)
        {
            if (Guid.TryParse(commandExecutionUuid.Value, out Guid commandId))
            {
                return GetCommand(commandId);
            }
            var message = $"CommandExecutionUUID '{commandExecutionUuid}' is not a valid GUID !";
            _logger.LogWarning(message);
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid, message));
            return null;
        }

        /// <summary>
        /// Registers response stream for command execution info updates for a given command UUID
        /// </summary>
        /// <param name="cmdExecId">Request containing command ID to register</param>
        /// <param name="responseStream">client response stream to register command execution info updates to</param>
        /// <param name="cancellationToken">cancellation token from open client connection</param>
        /// <returns></returns>
        public async Task RegisterForInfo(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, CancellationToken cancellationToken)
        {
            // create own CancellationTokenSource to be used by the server
            var serverSideCancellationTokenSource = new CancellationTokenSource();

            var command = GetCommand(cmdExecId);
            await command.AddInfoObserver(responseStream, cancellationToken, serverSideCancellationTokenSource);
            _logger.LogDebug($"Added Info subscription of command execution {cmdExecId}");

            // if command has already been finished return immediately
            if (GetCommand(cmdExecId).IsCompleted)
            {
                return;
            }
            
            try
            {
                while (!cancellationToken.IsCancellationRequested && !serverSideCancellationTokenSource.Token.IsCancellationRequested)
                {
                    GetCommand(cmdExecId);
                    await Task.Delay(Constants.EXECUTIONINFO_DELAY_IN_MILLISECONDS, cancellationToken);
                }
            }
            catch (TaskCanceledException)
            {
                _logger.LogDebug($"Cancelled command: {cmdExecId.Value}");
            }
        }

        public async Task ProcessIntermediateResponses<TIntermediateResponse>(CommandExecutionUUID cmdExecId, IServerStreamWriter<TIntermediateResponse> responseStream, Action<TParameter, CancellationToken> intermediateResponseHandler, CancellationToken token)
        {
            var command = GetCommand(cmdExecId);
            await command.ProcessIntermediateResponses(responseStream, intermediateResponseHandler, token);
        }

        /// <summary>
        /// Cleans up all observable command wrappers held by the manager. Tasks will be cancelled.
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    foreach(var cmd in _commands)
                    {
                        cmd.Value.CommandExpired -= RemoveExpiredCommand;
                        cmd.Value.Dispose();
                    }
                }
                _disposedValue = true;
            }
        }

        private ObservableCommandWrapper<TParameter, TResponse> GetCommand(Guid commandId)
        {
            if (!_commands.TryGetValue(commandId, out var command))
            {
                var message = $"No such command with ID {commandId} was found";
                _logger.LogWarning(message);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid,
                    message));
            }

            return command;
        }

        private Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(ObservableCommandWrapper<TParameter, TResponse> command)
        {
            if (_commands.TryAdd(command.CommandExecutionUuid, command))
            {
                _logger.LogDebug($"Executing command: {command.CommandExecutionUuid}");
                _ = command.Execute(_commandAutoResetEvent);
                if (command.IsQueued)
                {
                    while (_commands.Any(x => !x.Value.IsCompleted && x.Value.CommandExecutionUuid != command.CommandExecutionUuid))
                    {
                        Task.Delay(TimeSpan.FromMilliseconds(10)).Wait();
                    }
                }
                _commandAutoResetEvent.Set();

                command.CommandExpired += RemoveExpiredCommand;
                return Task.FromResult(command);
            }
            else
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.CommandExecutionNotAccepted,
                    $"Command with Id {command.CommandExecutionUuid} could not be added!"));
                throw new InvalidOperationException();
            }
        }

        private void RemoveExpiredCommand(object sender, CommandExpiredArgs args)
        {
            if (_commands.TryRemove(args.CommandId, out var command))
            {
                command.Dispose();
                _logger.LogInformation($"Disposed expired command: {args.CommandId}");
            }
            else
            {
                _logger.LogWarning($"Could not remove command with ID {args.CommandId}");
            }
        }
    }
}
