﻿using Microsoft.Extensions.Logging;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SiLA2.Server
{
    public class SiLA2Server : ISiLA2Server
    {
        /// <summary>The list of implemented features.</summary>
        private readonly Dictionary<string, Feature> _implementedFeatures;
        private readonly IServiceAnnouncer _serviceAnnouncer;
        private readonly ServerInformation _serverInformation;
        private readonly MetadataManager _metadataManager;
        private readonly ILogger<SiLA2Server> _logger;

        public ServerInformation ServerInformation => _serverInformation;

        public MetadataManager MetadataManager => _metadataManager;

        public SiLA2Server(IServiceAnnouncer serviceAnnouncer, ServerInformation serverInformation, MetadataManager metadataManager, ILogger<SiLA2Server> logger)
        {
            _implementedFeatures = new Dictionary<string, Feature>();
            _serviceAnnouncer = serviceAnnouncer;
            _serverInformation = serverInformation;
            _metadataManager = metadataManager;
            _logger = logger;

            ReadFeature("SiLAService-v1_0.sila.xml", typeof(SiLAService));
        }

        #region Feature discovery

        /// <summary>
        /// Method for extracting a <see cref="Feature"/> from an EmbeddedResource.
        /// </summary>
        /// <param name="resourceName">The name of the embedded resource.
        ///
        /// The pattern is: '[assembly default namespace].[directory].[filename]'
        /// For example: 'sila2.features.SiLAService.xml'.
        /// </param>
        /// <param name="implementationType">A type from the assembly that contains the EmbeddedResource.</param>
        /// <exception cref="FileNotFoundException">Thrown when the embedded resource stream can't be found.</exception>
        /// <exception cref="ApplicationException">Thrown when reading the feature from the Stream fails.</exception>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string resourceName, Type implementationType)
        {
            var fullyQualifiedResourceName = implementationType.Module.Assembly.GetManifestResourceNames()
                .Single(name => name.EndsWith(resourceName));
            var featureStream = implementationType.Module.Assembly.GetManifestResourceStream(fullyQualifiedResourceName);
            // get the embedded resource from the assembly of the implementation
            if (featureStream == null)
            {
                var resourcesJointList =
                    string.Join(", ", implementationType.Module.Assembly.GetManifestResourceNames());
                throw new FileNotFoundException($"Feature resource '{resourceName}' was not found" +
                                                $" in the list of embedded resources: [{resourcesJointList}]." +
                                                " Did you set its BuildAction to 'EmbeddedResource'?");
            }

            try
            {
                var feature = FeatureGenerator.ReadFeatureFromStream(featureStream);
                _implementedFeatures[feature.FullyQualifiedIdentifier] = feature;
                return feature;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to parse Xml from embedded resource stream.");
                throw new ApplicationException("Unable to parse Xml from embedded resource stream.", ex);
            }
        }

        /// <summary>
        /// Reads and deserializes the feature definition from the given file.
        /// (it simply does the call 'GrpcServer.Services.Add([grpc_stub_class_name].BindService(new [feature_implementation_class_name(this, [implemented_feature_object])))' )
        /// </summary>
        /// <param name="featureDefinitionFile">The file containing the XML Feature Definition.</param>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string featureDefinitionFile)
        {
            // deserialize the given feature defintion
            Feature feature;
            if (Uri.IsWellFormedUriString(featureDefinitionFile, UriKind.Absolute))
            {
                feature = FeatureGenerator.ReadFeatureFromOnlineResource(featureDefinitionFile);
            }
            else
            {
                feature = FeatureGenerator.ReadFeatureFromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, featureDefinitionFile));
            }

            _implementedFeatures[feature.FullyQualifiedIdentifier] = feature;

            return feature;
        }

        /// <summary>
        /// Gets the feature object with the given feature identifier.
        /// </summary>
        /// <param name="featureIdentifier">The (full qualified) feature identifier.</param>
        /// <returns>The feature object.</returns>
        public Feature GetFeature(string featureIdentifier)
        {
            if (_implementedFeatures.ContainsKey(featureIdentifier))
            {
                return _implementedFeatures[featureIdentifier];
            }
            return null;
        }

        /// <summary>
        /// Gets the feature object that belongs to the given fully qualified element identifier.
        /// Elements can be:
        ///  * command
        ///  * command parameter
        ///  * command response
        ///  * intermediate command response
        ///  * execution error
        ///  * property
        ///  * data type
        ///  * metadata identifier
        /// </summary>
        /// <param name="fullyQualifiedIdentifier">The full qualified identifier of the feature element.</param>
        /// <returns>The feature object containing the specified parameter.</returns>
        public Feature GetFeatureOfElement(string fullyQualifiedIdentifier)
        {
            return _implementedFeatures.FirstOrDefault(f => fullyQualifiedIdentifier.StartsWith(f.Key)).Value;
        }

        public void Start()
        {
            _serviceAnnouncer.Start();
        }

        /// <summary>
        /// Gets a list of fully qualified identifiers of the implemented features.
        /// </summary>
        public List<string> ImplementedFeatures => _implementedFeatures.Keys.ToList();

        #endregion
    }
}
