﻿using SiLA2.Server.Utils;
using System;
using System.Collections.Generic;

namespace SiLA2.Server
{
    public sealed class AuthorizationTokenRepository
    {
        private class Nested
        {
            public static IDictionary<Guid, AuthorizationTokenItem> TokenMap = new Dictionary<Guid, AuthorizationTokenItem>();

            // Explicit static constructor to tell C# compiler not to mark type as beforefieldinit
            static Nested() { }

            internal static readonly AuthorizationTokenRepository Instance = new AuthorizationTokenRepository();
        }

        private class AuthorizationTokenItem
        {
            public DateTime ExpiryDate { get; }
            public IEnumerable<string> Identifiers { get; }

            public AuthorizationTokenItem(int seconds, IEnumerable<string> identifiers)
            {
                ExpiryDate = DateTime.Now.AddSeconds(seconds);
                Identifiers = identifiers;
            }
        }

        public static AuthorizationTokenRepository Instance => Nested.Instance;

        private AuthorizationTokenRepository() {}

        public void AddToken(Guid token, int validitySeconds, IEnumerable<string> identifiers)
        {
            if (!Nested.TokenMap.ContainsKey(token))
            {
                Nested.TokenMap.Add(token, new AuthorizationTokenItem(validitySeconds, identifiers));
                return;
            }
            ErrorHandling.HandleException(new InvalidOperationException($"TokenMap already contains token '{token}'"));
        }

        public void RemoveToken(Guid token)
        {
            if(Nested.TokenMap.ContainsKey(token))
            {
                Nested.TokenMap.Remove(token);
            }
        }

        public bool IsTokenValid(Guid token)
        {
            if(!Nested.TokenMap.ContainsKey(token))
                return false;
            else
            {
                if (Nested.TokenMap[token].ExpiryDate > DateTime.Now)
                    return true;
                else
                {
                    Nested.TokenMap.Remove(token);
                    return false;
                }
            }
        }

        public bool TokenExists(Guid token)
        {
            return Nested.TokenMap.ContainsKey(token);
        }
    }
}
