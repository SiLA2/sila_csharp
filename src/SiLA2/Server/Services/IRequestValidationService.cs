﻿using Google.Protobuf;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public interface IRequestValidationService
    {
        Task ValidateCommandParameters<T>(T request, Feature feature, string cmdId, string nspace = Utils.Constants.SILA_ORG_SILASTANDARD) where T : IMessage<T>;
    }
}
