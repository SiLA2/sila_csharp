﻿using Grpc.Core;
using Microsoft.Extensions.Configuration;
using SiLAFramework = Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Core.Authenticationservice.V1;
using SiLA2.Server.Utils;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Sila2.Org.Silastandard.Core.Authenticationservice.V1.AuthenticationService;
using SiLA2.Utils;

namespace SiLA2.Server.Services
{
    public class AuthenticationService : AuthenticationServiceBase
    {
        private const string FullyQualifiedFeatureIdentifier = "org.silastandard/core/AuthenticationService/v1";

        private readonly IAuthenticationInspector _authenticationInspector;
        private readonly TimeSpan _tokenValidity;

        public Feature SiLAFeature { get; }

        public AuthenticationService(ISiLA2Server siLA2Server, IAuthenticationInspector authenticationInspector, IConfiguration configuration)
        {
            _authenticationInspector = authenticationInspector;
            _tokenValidity = TimeSpan.FromSeconds(int.Parse(configuration["AuthTokenValidityInSeconds"]));
            SiLAFeature = siLA2Server.GetFeature(FullyQualifiedFeatureIdentifier) ?? siLA2Server.ReadFeature(Path.Combine("Features", "AuthenticationService-v1_0.sila.xml"));
        }

        public override async Task<Login_Responses> Login(Login_Parameters request, ServerCallContext context)
        {
            if (request.UserIdentification == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Login", "UserIdentification"),
                    "UserIdentification not provided"));
            }
            Validation.ValidateParameter(request.UserIdentification.Value, SiLAFeature, nameof(Login), "UserIdentification");

            if (request.Password == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Login", "Password"),
                    "Password not provided"));
            }
            Validation.ValidateParameter(request.Password.Value, SiLAFeature, nameof(Login), "Password");

            if (request.RequestedServer == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Login", "RequestedServer"),
                    "RequestedServer not provided"));
            }
            Validation.ValidateParameter(request.RequestedServer.Value.ToLowerInvariant(), SiLAFeature, nameof(Login), "RequestedServer");

            if (request.RequestedFeatures != null && request.RequestedFeatures.Count > 0)
            {
                foreach (var feature in request.RequestedFeatures)
                {
                    if (!Regex.IsMatch(feature.Value, Constants.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX, RegexOptions.IgnoreCase))
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Login", "RequestedFeatures"),
                            $"The RequestedFeature does not match its Regular Expression '{Constants.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}' !"));
                    }
                }
            }

            if (await _authenticationInspector.IsAuthenticated(request))
            {
                Guid guid = Guid.NewGuid();
                AuthorizationTokenRepository.Instance.AddToken(guid, (int)_tokenValidity.TotalSeconds, request.RequestedFeatures.Select(x => x.Value));
                return new Login_Responses { AccessToken = new SiLAFramework.String { Value = guid.ToString() }, TokenLifetime = new SiLAFramework.Integer { Value = (int)_tokenValidity.TotalSeconds } };
            }
            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier("AuthenticationFailed"),
                $"Could not authenticate user '{request.UserIdentification.Value}'"));
            return new Login_Responses();
        }

        public override Task<Logout_Responses> Logout(Logout_Parameters request, ServerCallContext context)
        {
            if (request.AccessToken == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Logout", "AccessToken"),
                    "AccessToken not provided"));
            }

            if (!Guid.TryParse(request.AccessToken.Value, out Guid accessToken))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("Logout", "AccessToken"),
                    "AccessToken is not a valid Guid"));
            }
            else
            {
                if (!AuthorizationTokenRepository.Instance.TokenExists(accessToken))
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier("InvalidAccessToken"),
                        "Unknown AccessToken"));
                }
                AuthorizationTokenRepository.Instance.RemoveToken(accessToken);
            }

            Logout_Responses logout_Responses = new Logout_Responses();
            return Task.FromResult(logout_Responses);
        }
    }
}
