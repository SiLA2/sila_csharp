﻿using Google.Protobuf;
using Grpc.Core;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public class SiLABinaryDownloadService : BinaryDownload.BinaryDownloadBase
    {
        private readonly IBinaryDownloadRepository _binaryDownloadRepository;

        public SiLABinaryDownloadService(IBinaryDownloadRepository binaryDownloadRepository)
        {
            _binaryDownloadRepository = binaryDownloadRepository;
        }

        #region Overrides of BinaryDownloadBase

        public override async Task<GetBinaryInfoResponse> GetBinaryInfo(GetBinaryInfoRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = await _binaryDownloadRepository.CheckBinaryDownloadTransferUUID(request.BinaryTransferUUID);

            return new GetBinaryInfoResponse
            {
                BinarySize = (ulong)_binaryDownloadRepository.DownloadDataMap[binaryTransferUUID].Length,
                LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
            };
        }

        public override async Task GetChunk(IAsyncStreamReader<GetChunkRequest> requestStream, IServerStreamWriter<GetChunkResponse> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext(context.CancellationToken))
            {
                var binaryTransferUUID = await _binaryDownloadRepository.CheckBinaryDownloadTransferUUID(requestStream.Current.BinaryTransferUUID);

                // check for maximum chunk size of 2 MiB
                if ((int)requestStream.Current.Length > 2 * 1024 * 1024)
                {
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryDownloadFailed,
                        $"The requested chunk size ({(int)requestStream.Current.Length}) is out of bounds (maximum chunk size is 2 MiB)");
                }

                // check requested chunk size against available data
                if ((int)requestStream.Current.Offset + (int)requestStream.Current.Length > _binaryDownloadRepository.DownloadDataMap[binaryTransferUUID].Length)
                {
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryDownloadFailed,
                        $"The requested chunk size ({(int)requestStream.Current.Length}) is out of bounds (size of available data exceeded)");
                }

                // get data chunk
                var chunk = ByteString.CopyFrom(_binaryDownloadRepository.DownloadDataMap[binaryTransferUUID], (int)requestStream.Current.Offset, (int)requestStream.Current.Length);

                await responseStream.WriteAsync(new GetChunkResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    Offset = requestStream.Current.Offset,
                    Payload = chunk,
                    LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                });
            }
        }

        public override async Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = await _binaryDownloadRepository.CheckBinaryDownloadTransferUUID(request.BinaryTransferUUID);

            _binaryDownloadRepository.DownloadDataMap.TryRemove(binaryTransferUUID, out byte[] _);   ////TODO. check whether this crashes with a parallel running binary download

            return new DeleteBinaryResponse();
        }

        #endregion
    }
}
