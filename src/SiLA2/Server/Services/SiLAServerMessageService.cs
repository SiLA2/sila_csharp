﻿using Google.Protobuf;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Server.Services
{
    public class SiLAServerMessageService : ISiLAServerMessageService
    {
        private readonly ISiLA2Server _siLA2Server;

        public ConcurrentQueue<SiLAServerMessage> ServerMessageResponses { get; } = new ConcurrentQueue<SiLAServerMessage>();

        public SiLAServerMessageService(ISiLA2Server siLA2Server)
        {
            _siLA2Server = siLA2Server;
        }

        [Description("Work in progress...this method needs to be overwritten !")]
        public virtual Task<SiLAServerMessage> ProcessSilaClientRequest(SiLAClientMessage siLAClientMessage)
        {
            throw new NotImplementedException("TODO: Needs to be implemented...please overwrite this method 'public virtual Task<SiLAServerMessage> ProcessSilaClientRequest(SiLAClientMessage siLAClientMessage)' !");

            SiLAServerMessage returnMessage = null;
            //await Task.Run(() => {
            //    //TODO: To be implemented
            //    ServerMessageResponses.Enqueue(new SiLAServerMessage { RequestUUID = siLAClientMessage.RequestUUID });
            //});
            if (siLAClientMessage.UnobservablePropertyRead is UnobservablePropertyRead propertyToRead)
            {
                returnMessage = new SiLAServerMessage { UnobservablePropertyValue = new UnobservablePropertyValue() { Value = ByteString.CopyFromUtf8("Change me!")} };
            }
            else if (siLAClientMessage.ObservablePropertySubscription is ObservablePropertySubscription propertyToSubscribe)
            {
                returnMessage = new SiLAServerMessage { ObservablePropertyValue = new ObservablePropertyValue() { Value = ByteString.CopyFromUtf8("Change me observable!") } };
            }
            //else
            //{
            //    ErrorHandling.HandleException(new System.Exception("SiLA message unknown."));
            //}

            returnMessage.RequestUUID = siLAClientMessage.RequestUUID;
            return Task.FromResult(returnMessage);
        }
    }
}