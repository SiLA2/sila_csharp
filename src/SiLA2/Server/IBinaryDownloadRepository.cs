﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public interface IBinaryDownloadRepository
    {
        ConcurrentDictionary<Guid, byte[]> DownloadDataMap { get; }
        Task<Guid> CheckBinaryDownloadTransferUUID(string binaryTransferUUIDString);
    }
}