﻿using SiLA2.Server.Services;
using System;
using System.Collections.Generic;

namespace SiLA2.Server
{
    public interface ISiLA2Server
    {
        ServerInformation ServerInformation { get; }
        MetadataManager MetadataManager { get; }
        List<string> ImplementedFeatures { get; }
        void Start();
        Feature GetFeature(string featureIdentifier);
        Feature ReadFeature(string featureDefinitionFile);
        Feature ReadFeature(string resourceName, Type implementationType);
        Feature GetFeatureOfElement(string fullyQualifiedIdentifier);
    }
}