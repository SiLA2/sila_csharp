﻿using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace SiLA2.Server
{
    public class BinaryDownloadRepository : IBinaryDownloadRepository
    {
        public ConcurrentDictionary<Guid, byte[]> DownloadDataMap { get; } = new();

        public async Task<Guid> CheckBinaryDownloadTransferUUID(string binaryTransferUUIDString)
        {
            return await Task.Run(() =>
            {
                if (!Guid.TryParse(binaryTransferUUIDString, out var binaryTransferUUID) || !DownloadDataMap.ContainsKey(binaryTransferUUID))
                {
                    ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.InvalidBinaryTransferUuid,
                        $"The given Binary Download Transfer UUID '{binaryTransferUUIDString}' is not valid");
                }

                return binaryTransferUUID;
            });
        }
    }
}