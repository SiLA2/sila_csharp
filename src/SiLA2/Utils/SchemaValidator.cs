﻿using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.Collections.Generic;
using System.Net;
using System;

namespace SiLA2.Utils
{
    public class SchemaValidator
    {
        public static bool ValidateAgainstXmlSchema(string xmlString, string xsdMarkup, out string errorMessage)
        {
            errorMessage = string.Empty;

            var settings = new XmlReaderSettings
            {
                DtdProcessing = DtdProcessing.Parse,
                MaxCharactersFromEntities = 1024,
                ValidationType = ValidationType.Schema
            };

            // get target namespace
            //TODO

            try
            {
                var schemas = new XmlSchemaSet();
                schemas.Add("", XmlReader.Create(new StringReader(xsdMarkup), settings));

                var errors = false;
                var message = string.Empty;
                XDocument.Parse(xmlString).Validate(schemas, (_, e) =>
                {
                    message = e.Message;
                    errors = true;
                });

                if (!errors)
                {
                    return true;
                }

                errorMessage = message;
            }
            catch (Exception ex)
            {
                var s = ex.Message;
            }

            return false;
        }

        public static bool ValidateAgainstXmlSchemaUrl(string xmlString, string schemaUrl, out string errorMessage)
        {
            var wc = new WebClient();
            var xsdContent = wc.DownloadString(schemaUrl);

            return ValidateAgainstXmlSchema(xmlString, xsdContent, out errorMessage);
        }

        public static bool ValidateAgainstJsonSchema(string jsonString, string jsonSchema, out string errorMessage)
        {
            errorMessage = string.Empty;

            var schema = JsonSchema.Parse(jsonSchema);
            var content = JObject.Parse(jsonString);

            IList<string> messages;
            if (content.IsValid(schema, out messages)) { return true; }

            errorMessage = string.Join("\n", messages);
            return false;
        }

        public static bool ValidateAgainstJsonSchemaUrl(string jsonString, string schemaUrl, out string errorMessage)
        {
            string jsonSchema;
            using (var streamReader = new StreamReader(schemaUrl, Encoding.UTF8))
            {
                jsonSchema = streamReader.ReadToEnd();
            }

            return ValidateAgainstJsonSchema(jsonString, jsonSchema, out errorMessage);
        }
    }
}
