﻿using System;
using System.Linq;
using System.Text;

namespace SiLA2.Domain
{
    public static class FeatureDefinitionHelper
    {
        public static string ShowFeatureInfos(Feature feature)
        {
            var sb = new StringBuilder();

            var properties = feature.GetDefinedProperties();
            var commands = feature.GetDefinedCommands();
            var definedDataTypes = feature.GetDefinedDataTypes();
            var metaDatas = feature.GetDefinedMetadata();
            var executionErrors = feature.GetDefinedExecutionErrors();

            sb.AppendLine($"{Environment.NewLine}=== {feature.FullyQualifiedIdentifier} ==={Environment.NewLine}");
            sb.AppendLine($"Indentfier: {feature.Identifier}");
            sb.AppendLine($"Display Name: {feature.DisplayName}");
            sb.AppendLine($"Description: {feature.Description}");
            sb.AppendLine($"SiLA2 Version: {feature.SiLA2Version}");
            sb.AppendLine($"Feature Version: {feature.FeatureVersion}");
            sb.AppendLine($"Locale: {feature.Locale}");
            sb.AppendLine($"Maturity Level: {feature.MaturityLevel}");
            sb.AppendLine($"Originator: {feature.Originator}");
            sb.AppendLine($"Category: {feature.Originator}");
            sb.AppendLine($"Namespace: {feature.Namespace}");
            sb.AppendLine($"FullyQualifiedIdentifier: {feature.FullyQualifiedIdentifier}");

            if (properties.Count > 0)
            {
                sb.AppendLine($"{Environment.NewLine}Feature Properties");
                sb.AppendLine("~~~~~~~~~~~~~~~~~~");
                foreach (var item in properties)
                {
                    sb.AppendLine($"Property Identifier: {item.Identifier}");
                    sb.AppendLine($"Property Is Observable: {item.Observable}");
                    string id = item.Identifier;
                    sb.AppendLine($"Property DataType: {GetDataType(item.DataType, feature, ref id)}");
                    sb.AppendLine($"Property Description: {item.Description}");
                    sb.AppendLine("---");
                }
            }

            if (commands.Count > 0)
            {
                sb.AppendLine($"{Environment.NewLine}Feature Commands");
                sb.AppendLine("~~~~~~~~~~~~~~~~");
                foreach (var item in commands)
                {
                    sb.AppendLine($"Command Identifier: {item.Identifier}");
                    sb.AppendLine($"Command Is Observable: {item.Observable}");
                    sb.AppendLine($"Command Description: {item.Description}");

                    if (item.Parameter != null)
                    {
                        foreach (var parameter in item.Parameter)
                        {
                            sb.AppendLine($"Parameter Identifier: {parameter.Identifier}");
                            string id = parameter.Identifier;
                            sb.AppendLine($"Parameter DataType: {GetDataType(parameter.DataType, feature, ref id)}");
                            sb.AppendLine($"Parameter Description: {item.Description}");
                        }
                    }

                    if (item.Response != null)
                    {
                        foreach (var response in item.Response)
                        {
                            sb.AppendLine($"Response Identifier: {response.Identifier}");
                            string id = response.Identifier;
                            sb.AppendLine($"Response DataType: {GetDataType(response.DataType, feature, ref id)}");
                            sb.AppendLine($"Response Description: {item.Description}");
                        }
                    }

                    if (item.IntermediateResponse != null)
                    {
                        foreach (var response in item.IntermediateResponse)
                        {
                            sb.AppendLine($"Intermediate Response Identifier: {response.Identifier}");
                            string id = response.Identifier;
                            sb.AppendLine($"Intermediate Response DataType: {GetDataType(response.DataType, feature, ref id)}");
                            sb.AppendLine($"Intermediate Response Description: {item.Description}");
                        }
                    }

                    if (item.DefinedExecutionErrors != null)
                    {
                        foreach (var definedExecutionError in item.DefinedExecutionErrors)
                        {
                            sb.AppendLine($"DefinedExecutionError: {definedExecutionError}");
                        }
                    }
                    sb.AppendLine("---");
                }
            }

            if (definedDataTypes.Count > 0)
            {
                sb.AppendLine($"{Environment.NewLine}Feature DefinedDataTypes");
                sb.AppendLine("~~~~~~~~~~~~~~~~~~~~~~~~");
                foreach (var item in definedDataTypes)
                {
                    sb.AppendLine($"DefinedDataTypes Identifier: {item.Identifier}");
                    string id = item.Identifier;
                    sb.AppendLine($"DefinedDataTypes DataType: {GetDataType(item.DataType, feature, ref id)}");
                    sb.AppendLine($"Defined DataType Description: {item.Description}");
                    sb.AppendLine("---");
                }
            }

            if (metaDatas.Count > 0)
            {
                sb.AppendLine($"{Environment.NewLine}Feature MetaData");
                sb.AppendLine("~~~~~~~~~~~~~~~~");
                foreach (var item in metaDatas)
                {
                    sb.AppendLine($"MetaData Identifier: {item.Identifier}");
                    string id = item.Identifier;
                    sb.AppendLine($"MetaData DataType: {GetDataType(item.DataType, feature, ref id)}");
                    sb.AppendLine($"MetaData Description: {item.Description}");
                    sb.AppendLine("---");
                }
            }

            if (executionErrors.Count > 0)
            {
                sb.AppendLine($"{Environment.NewLine}Feature ExecutionErrors");
                sb.AppendLine("~~~~~~~~~~~~~~~~~~~~~~~");
                foreach (var item in executionErrors)
                {
                    sb.AppendLine($"ExecutionErrors Identifier: {item.Identifier}");
                    sb.AppendLine($"ExecutionErrors Description: {item.Description}");
                    sb.AppendLine("---");
                }
            }

            sb.AppendLine($"{Environment.NewLine}=====================================================");

            return sb.ToString();
        }

        public static string GetDataType(DataTypeType dataType, Feature feature, ref string property, string structureIdentifier = null)
        {
            if (dataType.Item.GetType() == typeof(BasicType))
            {
                var basicType = Enum.Parse(typeof(BasicType), dataType.Item.ToString());
                switch (basicType)
                {
                    case BasicType.String:
                        return typeof(Sila2.Org.Silastandard.String).ToString();
                    case BasicType.Binary:
                        return typeof(Sila2.Org.Silastandard.Binary).ToString();
                    case BasicType.Boolean:
                        return typeof(Sila2.Org.Silastandard.Boolean).ToString();
                    case BasicType.Date:
                        return typeof(Sila2.Org.Silastandard.Date).ToString();
                    case BasicType.Real:
                        return typeof(Sila2.Org.Silastandard.Real).ToString();
                    case BasicType.Integer:
                        return typeof(Sila2.Org.Silastandard.Integer).ToString();
                    case BasicType.Time:
                        return typeof(Sila2.Org.Silastandard.Time).ToString();
                    case BasicType.Timestamp:
                        return typeof(Sila2.Org.Silastandard.Timestamp).ToString();
                    case BasicType.Any:
                        return typeof(Sila2.Org.Silastandard.Any).ToString();
                    default:
                        throw new ArgumentException($"Unknown BasicType '{basicType}'");
                }

            }
            else if (dataType.Item.GetType() == typeof(ConstrainedType))
            {
                string constrained = $"{property}/Constrained/";
                return GetDataType(((ConstrainedType)dataType.Item).DataType, feature, ref constrained);
            }
            else if (dataType.Item.GetType() == typeof(ListType))
            {
                var listItemType = GetDataType(((ListType)dataType.Item).DataType, feature, ref property);
                return $"List<{listItemType}>";
            }
            else if (dataType.Item.GetType() == typeof(StructureType))
            {
                var elements = ((StructureType)dataType.Item).Element;

                var sb = new StringBuilder();
                foreach (var element in elements)
                {
                    sb.Append($"({element.Identifier} : {GetDataType(element.DataType, feature, ref property)})");
                }

                return $"Structure [{sb}]";
            }
            else if (dataType.Item is string)
            {
                var definedDataTypes = feature.GetDefinedDataTypes();
                string id = dataType.Item.ToString();
                var definedType = definedDataTypes.SingleOrDefault(t => t.Identifier.Equals(id));

                if (definedType != null)
                {
                    return GetDataType(definedType.DataType, feature, ref property, definedType.Identifier);
                }
                throw new ArgumentException($"Unknown DefinedDataType '{dataType.Item}'");
            }

            throw new ArgumentException($"Unknown DataType '{dataType.Item.GetType()}'");
        }
    }
}
