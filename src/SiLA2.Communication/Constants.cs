﻿namespace SiLA2.Communication;

internal static class Constants
{
    public const string DYNAMIC_PROTOBUF_REQUEST = "DynamicProtobufRequest";
    public const string DYNAMIC_PROTOBUF_RESPONSE = "DynamicProtobufResponse";
}
