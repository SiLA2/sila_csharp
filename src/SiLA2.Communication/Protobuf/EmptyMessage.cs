﻿using ProtoBuf;

namespace SiLA2.Communication.Protobuf
{
    [ProtoContract]
    public sealed class EmptyMessage { }
}
