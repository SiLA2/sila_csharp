﻿namespace SiLA2.Communication.Services;

public interface ICommandPayloadProvider
{
    IDictionary<string, object> GetPayloadMap(IDictionary<Tuple<string, string, string>, string> parameterMap, FeatureCommand command, Feature feature);
}
