﻿namespace SiLA2.Communication.Services
{
    public interface IPayloadFactory
    {
        Tuple<Type, Type> GetPropertyPayloadTypes(Feature feature, string operation);
        Tuple<Type, Type> GetCommandPayloadTypes(Feature feature, string operation);
        Tuple<Type, Type, Type> GetCommandPayloadTypesWithIntermediateCommandResponseType(Feature feature, string operation);
    }
}
