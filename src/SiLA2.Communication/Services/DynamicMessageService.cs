﻿using Grpc.Net.Client;
using ProtoBuf.Grpc.Client;

using Sila2.Org.Silastandard;
using System.Reflection;

namespace SiLA2.Communication.Services
{
    public class DynamicMessageService : IDynamicMessageService
    {
        private readonly IPayloadFactory _payloadFactory;

        public DynamicMessageService(IPayloadFactory payloadFactory)
        {
            _payloadFactory = payloadFactory;
        }

        public object GetUnobservableProperty(string propertyName, GrpcChannel channel, Feature feature)
        {
            var payloadTypes = _payloadFactory.GetPropertyPayloadTypes(feature, propertyName);

            MethodInfo genericMethod = GetUnaryMethod(payloadTypes);
            GrpcClient client = GetClient(channel, feature);

            object request = Activator.CreateInstance(payloadTypes.Item1);

            return genericMethod.Invoke(client, [request, $"Get_{propertyName}", null]);
        }

        public IAsyncEnumerable<object> SubcribeObservableProperty(string propertyName, GrpcChannel channel, Feature feature)
        {
            var payloadTypes = _payloadFactory.GetPropertyPayloadTypes(feature, propertyName);

            MethodInfo genericMethod = GetServerStreamingMethod(payloadTypes);
            GrpcClient client = GetClient(channel, feature);

            object request = Activator.CreateInstance(payloadTypes.Item1);

            return (IAsyncEnumerable<object>)genericMethod.Invoke(client, [request, $"Subscribe_{propertyName}", null]);
        }

        public object ExecuteUnobservableCommand(string operationName, GrpcChannel channel, Feature feature, IDictionary<string, object> payloadMap = null)
        {
            var payloadTypes = _payloadFactory.GetCommandPayloadTypes(feature, operationName);

            MethodInfo genericMethod = GetUnaryMethod(payloadTypes);
            GrpcClient client = GetClient(channel, feature);

            object request = Activator.CreateInstance(payloadTypes.Item1);

            if (payloadMap != null)
            {
                foreach (var item in payloadMap)
                {
                    request.GetType().GetProperty(item.Key).SetValue(request, item.Value);
                }
            }

            return genericMethod.Invoke(client, [request, operationName, null]);
        }

        public Tuple<CommandConfirmation, IAsyncEnumerable<ExecutionInfo>, IAsyncEnumerable<object>, Type> ExecuteObservableCommand(string operationName, GrpcChannel channel, Feature feature, IDictionary<string, object> payloadMap = null)
        {
            var payloadTypes = _payloadFactory.GetCommandPayloadTypesWithIntermediateCommandResponseType(feature, operationName);

            var client = GetClient(channel, feature);
            MethodInfo registerObservableCommandMethod = GetUnaryMethod(new Tuple<Type, Type>(payloadTypes.Item1, typeof(CommandConfirmation)));

            object request = Activator.CreateInstance(payloadTypes.Item1);

            if (payloadMap != null)
            {
                foreach (var item in payloadMap)
                {
                    request.GetType().GetProperty(item.Key).SetValue(request, item.Value);
                }
            }

            var commandConfirmation = (CommandConfirmation)registerObservableCommandMethod.Invoke(client, [request, operationName, null]);

            var serverStreaming = client.ServerStreamingAsync<CommandExecutionUUID, ExecutionInfo>(commandConfirmation.CommandExecutionUUID, $"{operationName}_Info");

            var command = feature.GetDefinedCommands().SingleOrDefault(x => x.Identifier == operationName);
            IAsyncEnumerable<object> intermediateResponseStreaming = null;
            if (command.IntermediateResponse is not null)
            {
                var responseType = typeof(IAsyncEnumerable<>).MakeGenericType(payloadTypes.Item3);
                var methodInfo = client.GetType().GetMethods().Single(x => x.Name.StartsWith("ServerStreamingAsync") && x.GetParameters().Any(y => y.ParameterType == typeof(string))).MakeGenericMethod(typeof(CommandExecutionUUID), payloadTypes.Item3);
                var callContextType = typeof(ProtoBuf.Grpc.CallContext);
                var defaultCallContext = callContextType.GetProperty("Default", BindingFlags.Public | BindingFlags.Static)?.GetValue(null);
                intermediateResponseStreaming = (IAsyncEnumerable<object>)methodInfo.Invoke(client, [commandConfirmation.CommandExecutionUUID, $"{operationName}_Intermediate", defaultCallContext]);
            }

            return new Tuple<CommandConfirmation, IAsyncEnumerable<ExecutionInfo>, IAsyncEnumerable<object>, Type>(commandConfirmation, serverStreaming, intermediateResponseStreaming, payloadTypes.Item2);
        }

        public object GetObservableCommandResult(CommandExecutionUUID cmdId, string operationName, GrpcChannel channel, Feature feature, Type responseType)
        {
            var client = GetClient(channel, feature);
            MethodInfo observableCommandResultMethod = GetUnaryMethod(new Tuple<Type, Type>(typeof(CommandExecutionUUID), responseType));
            object request = Activator.CreateInstance(typeof(CommandExecutionUUID));
            request.GetType().GetProperty("Value").SetValue(request, cmdId.Value);
            return observableCommandResultMethod.Invoke(client, [request, $"{operationName}_Result", null]);
        }

        private MethodInfo GetUnaryMethod(Tuple<Type, Type> payloadTypes)
        {
            var method = typeof(GrpcClient).GetMethods().Single(x => x.Name.StartsWith("BlockingUnary") && x.GetParameters().Any(y => y.ParameterType == typeof(string)));
            return method.MakeGenericMethod(payloadTypes.Item1, payloadTypes.Item2);
        }

        private MethodInfo GetServerStreamingMethod(Tuple<Type, Type> payloadTypes)
        {
            var method = typeof(GrpcClient).GetMethods().Single(x => x.Name.StartsWith("ServerStreamingAsync") && x.GetParameters().Any(y => y.ParameterType == typeof(string)));
            return method.MakeGenericMethod(payloadTypes.Item1, payloadTypes.Item2);
        }

        private GrpcClient GetClient(GrpcChannel channel, Feature feature)
        {
            string grpcService = $"{feature.Namespace}.{feature.Identifier}";
            return new GrpcClient(channel.CreateCallInvoker(), grpcService);
        }
    }
}
