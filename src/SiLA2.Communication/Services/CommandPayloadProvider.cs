﻿using Google.Protobuf.Collections;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Sila2.Org.Silastandard.Protobuf;

namespace SiLA2.Communication.Services;

public class CommandPayloadProvider : ICommandPayloadProvider
{
    public IDictionary<string, object> GetPayloadMap(IDictionary<Tuple<string, string, string>, string> parameterMap, FeatureCommand command, Feature feature)
    {
        var payloadMap = new Dictionary<string, object>();
        var parameterKeys = parameterMap.Keys.Where(key => key.Item1 == feature.Identifier && key.Item2 == command.Identifier);

        foreach (var key in parameterKeys)
        {
            var cmdParameter = command.Parameter.Single(x => x.Identifier == key.Item3);

            object payloadValue = null;
            GetObject(cmdParameter.DataType, ref payloadValue, key, parameterMap, feature);

            payloadMap.Add(key.Item3, payloadValue);
        }

        return payloadMap;
    }

    private void GetObject(DataTypeType dataType, ref object obj, Tuple<string, string, string> key, IDictionary<Tuple<string, string, string>, string> parameterMap, Feature feature)
    {
        if (dataType.Item.GetType() == typeof(BasicType))
        {
            var basicType = Enum.Parse(typeof(BasicType), dataType.Item.ToString());

            switch (basicType)
            {
                case BasicType.String:
                    obj = new Sila2.Org.Silastandard.Protobuf.String { Value = parameterMap[key] };
                    break;
                case BasicType.Integer:
                    obj = new Integer { Value = long.Parse(parameterMap[key]) };
                    break;
                case BasicType.Boolean:
                    obj = new Sila2.Org.Silastandard.Protobuf.Boolean { Value = bool.Parse(parameterMap[key]) };
                    break;
                case BasicType.Real:
                    obj = new Real { Value = double.Parse(parameterMap[key]) };
                    break;
                case BasicType.Date:
                    obj = JsonConvert.DeserializeObject<Date>(parameterMap[key]);
                    break;
                case BasicType.Time:
                    obj = JsonConvert.DeserializeObject<Time>(parameterMap[key]);
                    break;
                case BasicType.Timestamp:
                    obj = JsonConvert.DeserializeObject<Timestamp>(parameterMap[key]);
                    break;
                case BasicType.Binary:
                    obj = JsonConvert.DeserializeObject<Binary>(parameterMap[key]);
                    break;
                case BasicType.Any:
                    obj = JsonConvert.DeserializeObject<Any>(parameterMap[key]);
                    break;
            }
        }
        else if (dataType.Item.GetType() == typeof(ConstrainedType))
        {
            GetObject(((ConstrainedType)dataType.Item).DataType, ref obj, key, parameterMap, feature);
        }
        else if (dataType.Item.GetType() == typeof(ListType))
        {
            var listElementType = ((ListType)dataType.Item).DataType.Item.ToString();
            switch (listElementType)
            {
                case "String":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Sila2.Org.Silastandard.Protobuf.String>>(parameterMap[key]);
                    break;
                case "Integer":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Integer>>(parameterMap[key]);
                    break;
                case "Boolean":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Sila2.Org.Silastandard.Protobuf.Boolean>>(parameterMap[key]);
                    break;
                case "Real":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Real>>(parameterMap[key]);
                    break;
                case "Date":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Date>>(parameterMap[key]);
                    break;
                case "Time":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Time>>(parameterMap[key]);
                    break;
                case "Timestamp":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Timestamp>>(parameterMap[key]);
                    break;
                case "Binary":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Binary>>(parameterMap[key]);
                    break;
                case "Any":
                    obj = JsonConvert.DeserializeObject<RepeatedField<Any>>(parameterMap[key]);
                    break;
                default:
                    throw new NotImplementedException($"List Element Type '{listElementType}' not supported !");
            }
        }
        else
        {
            var dataDefinition = dataType.Item.ToString();
            var featureElements = feature.Items.Where(x => x.GetType() == typeof(SiLAElement)).Cast<SiLAElement>();
            var structureTypes = featureElements.Where(x => x.DataType.Item.GetType() == typeof(StructureType));
            var targetDataDefinition = structureTypes.SingleOrDefault(x => x.Identifier == dataDefinition);
            if (targetDataDefinition != null)
            {
                JObject tmp = (JObject)JsonConvert.DeserializeObject(parameterMap[key]);

                StructureType structureType = (StructureType)targetDataDefinition.DataType.Item;

                Structure structure = new Structure { Element = [] };

                foreach (SiLAElement siLAElement in structureType.Element)
                {
                    //GetObject(siLAElement.DataType, ref obj, key, parameterMap, feature);

                    var property = FindPropertyByName(tmp, siLAElement.Identifier);
                    JObject tmpObj = JObject.Parse(property.Value.ToString());
                    switch (siLAElement.DataType.Item.ToString())
                    {
                        case "String":
                            var stringValue = new Sila2.Org.Silastandard.Protobuf.String { Value = tmpObj["Value"].ToString() };
                            structure.Element.Add(new Sila2.Org.Silastandard.Protobuf.SiLAElement { DataType = stringValue.Value, Identifier = siLAElement.Identifier, Description = siLAElement.Description, DisplayName = siLAElement.DisplayName  });
                            break;
                        case "Integer":
                            var integerValue = new Integer { Value = long.Parse(tmpObj["Value"].ToString()) };
                            structure.Element.Add(new Sila2.Org.Silastandard.Protobuf.SiLAElement { DataType = integerValue.Value.ToString(), Identifier = siLAElement.Identifier, Description = siLAElement.Description, DisplayName = siLAElement.DisplayName });
                            break;
                        case "Boolean":
                            var booleanValue = new Sila2.Org.Silastandard.Protobuf.Boolean { Value = bool.Parse(tmpObj["Value"].ToString()) };
                            structure.Element.Add(new Sila2.Org.Silastandard.Protobuf.SiLAElement { DataType = booleanValue.Value.ToString(), Identifier = siLAElement.Identifier, Description = siLAElement.Description, DisplayName = siLAElement.DisplayName });
                            obj = new Sila2.Org.Silastandard.Protobuf.Boolean { Value = bool.Parse(tmpObj["Value"].ToString()) };
                            break;
                        case "Real":
                            //obj = new Real { Value = double.Parse(parameterMap[key]) };
                            break;
                        case "Date":
                            //obj = JsonConvert.DeserializeObject<Date>(parameterMap[key]);
                            break;
                        case "Time":
                            //obj = JsonConvert.DeserializeObject<Time>(parameterMap[key]);
                            break;
                        case "Timestamp":
                            //obj = JsonConvert.DeserializeObject<Timestamp>(parameterMap[key]);
                            break;
                        case "Binary":
                            //obj = JsonConvert.DeserializeObject<Binary>(parameterMap[key]);
                            break;
                        case "Any":
                            //obj = JsonConvert.DeserializeObject<Any>(parameterMap[key]);
                            break;
                    }
                }

                obj = structure;

                throw new NotImplementedException();

                //var elements = ((StructureType)dataType.Item).Element;

                //var propertyMap = new Dictionary<string, Type>();
                //foreach (var element in elements)
                //{
                //    propertyMap.Add(element.Identifier, null);
                //    //propertyMap.Add(element.Identifier, GetObject(element.DataType, ref obj, key, parameterMap, feature));
                //}
                //TODO: Work in progress -> Test with EchoStructureValue in SiLA2.UniversalClient.Net ...

            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }

    JProperty? FindPropertyByName(JObject obj, string propertyName)
    {
        return obj.Descendants()
                  .OfType<JProperty>()
                  .FirstOrDefault(p => p.Name == propertyName);
    }
}
