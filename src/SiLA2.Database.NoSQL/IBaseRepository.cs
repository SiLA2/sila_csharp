﻿namespace SiLA2.Database.NoSQL
{
    public interface IBaseRepository<T>
    {
        T Create(T data, out object id);
        T Create(T data);
        IEnumerable<T> All();
        T FindById(object id);
        void Update(T entity);
        bool Delete(object id);
    }
}