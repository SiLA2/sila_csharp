﻿namespace Windows.Client.Shared
{
    public static class RegionNames
    {
        public const string CONTENT_REGION = "ContentRegion";
        public const string SERVER_CONTENT_REGION = "ServerContentRegion";
    }
}
