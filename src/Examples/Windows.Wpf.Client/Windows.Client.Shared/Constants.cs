﻿namespace Windows.Client.Shared
{
    public static class Constants
    {
        public static class CultureKeys
        {
            public static class Language
            {
                public const string LANGUAGE = "Language";
                public const string CHOSEN_LANGUAGE = "ChosenLanguage";
                public const string GERMAN = "de";
                public const string ENGLISH = "en";
            }

            public static class Common
            {
                public const string OK = "OK";
                public const string CANCEL = "Cancel";
                public const string YES = "Yes";
                public const string NO = "No";
                public const string CLOSE = "Close";
                public const string SAVE = "Save";
                public const string DELETE = "Delete";
                public const string EDIT = "Edit";
                public const string ADD = "Add";
                public const string REMOVE = "Remove";
                public const string SELECT = "Select";
                public const string SELECT_ALL = "SelectAll";
                public const string UNSELECT_ALL = "UnselectAll";
                public const string SEARCH = "Search";
                public const string FILTER = "Filter";
                public const string CLEAR = "Clear";
                public const string REFRESH = "Refresh";
                public const string BACK = "Back";
                public const string NEXT = "Next";
                public const string FINISH = "Finish";
            }
        }
    }
}
