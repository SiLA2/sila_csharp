﻿namespace Windows.Client.Shared;

public class Message<T>
{
    public T Payload { get; set; }

    public Message(T payload)
    {
        Payload = payload;
    }

    public override string ToString()
    {
        return $"{DateTime.Now} {Payload}";
    }
}
