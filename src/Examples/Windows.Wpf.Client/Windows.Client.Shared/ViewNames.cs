﻿namespace Windows.Client.Shared
{
    public static class ViewNames
    {
        public const string MAIN_VIEW = "MainView";
        public const string SERVER_CONTENT_VIEW = "ServerContentView";
    }
}
