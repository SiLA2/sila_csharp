﻿using SiLA2;

namespace Windows.Client.Shared.Services;

// TODO: Remove after SiLA2.Communication v.9.0.2 was published
public interface ICommandPayloadProvider
{
    IDictionary<string, object> GetPayloadMap(IDictionary<Tuple<string, string, string>, string> parameterMap, FeatureCommand command, Feature feature);
}
