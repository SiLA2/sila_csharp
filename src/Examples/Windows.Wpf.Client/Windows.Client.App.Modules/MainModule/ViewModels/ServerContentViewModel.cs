﻿using System.Collections.ObjectModel;

using Prism.Mvvm;

using SiLA2.Network.Discovery.mDNS;

using Windows.Client.App.Infrastructure.ViewModels;

namespace MainModule.ViewModels;

public class ServerContentViewModel : BindableBase
{
    private bool _isVisible;
    private ConnectionInfo _connectionInfo;

    public bool IsVisible { get => _isVisible; set => SetProperty(ref _isVisible, value); }
    public ConnectionInfo ConnectionInfo { get => _connectionInfo; set => SetProperty(ref _connectionInfo, value); }
    public ObservableCollection<FeatureViewModel> Features { get; } = [];
}
