﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using Grpc.Net.Client;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Prism.Commands;
using Prism.Events;
using Prism.Regions;

using Sila2.Org.Silastandard.Core.Silaservice.V1;

using SiLA2;
using SiLA2.Client.Dynamic;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server.Utils;
using SiLA2.Utils.gRPC;

using Windows.Client.App.Infrastructure;
using Windows.Client.App.Infrastructure.ViewModels;
using Windows.Client.Services;
using Windows.Client.Shared;
using Windows.Client.Shared.Services;

namespace MainModule.ViewModels;

public class MainViewModel : RegionViewModelBase
{
    private const string SERVER_SEARCH_TEXT = "Searching for Servers";
    private const string SERVER_SEARCH_FINISHED_TEXT = "SiLA2 Servers";
    private const int SEARCH_INTERVAL_IN_MS = 3000;
    private ICommand _searchServersCommand;
    private ICommand _copyMessageToClipboardCommand;
    private string _searchMessage = SERVER_SEARCH_TEXT;
    private readonly IDynamicConfigurator _dynamicConfigurator;
    private readonly IServiceFinder _serviceFinder;
    private readonly IGrpcChannelProvider _grpcChannelProvider;
    private readonly ICommandPayloadProvider _commandPayloadProvider;
    private readonly IEventAggregator _eventAggregator;
    private readonly IDispatcherService _dispatcherService;
    private readonly IConfiguration _configuration;
    private readonly ILogger<MainViewModel> _logger;
    private readonly string _serviceName;
    private readonly string _networkInterface;
    private bool _isSearching;
    private bool _isServerDataVisible;
    private string _selectedMessage;
    internal readonly ConcurrentDictionary<string, (ConnectionInfo, IDictionary<string, Feature>)> _connectionFeatureMap = [];

    public string SearchMessage
    {
        get => _searchMessage;
        set => SetProperty(ref _searchMessage, value);
    }

    public bool IsServerDataVisible { get => _isServerDataVisible; set => SetProperty(ref _isServerDataVisible, value); }

    public ObservableCollection<ConnenctionInfoViewModel> Servers { get; } = [];

    public ObservableCollection<string> Messages { get; } = [];

    public string SelectedMessage { get => _selectedMessage; set => SetProperty(ref _selectedMessage, value); }

    public ServerContentViewModel ServerContentViewModel { get; set; }

    public ICommand SearchForServers => _searchServersCommand ??= new DelegateCommand(SearchServers);

    public ICommand CopyMessageToClipboard => _copyMessageToClipboardCommand ??= new DelegateCommand(() =>
    {
        if (!string.IsNullOrEmpty(SelectedMessage))
        {
            Clipboard.SetText(SelectedMessage);
        }
    });

    public MainViewModel(IDynamicConfigurator dynamicConfigurator,
                         IServiceFinder serviceFinder,
                         IGrpcChannelProvider grpcChannelProvider,
                         ICommandPayloadProvider commandPayloadProvider,    
                         IEventAggregator eventAggregator,
                         IDispatcherService dispatcherService,
                         IConfiguration configuration,
                         IRegionManager regionManager,
                         ILoggerFactory loggerFactory,
                         ILogger<MainViewModel> logger) : base(regionManager)
    {
        _dynamicConfigurator = dynamicConfigurator;
        _serviceFinder = serviceFinder;
        _grpcChannelProvider = grpcChannelProvider;
        _commandPayloadProvider = commandPayloadProvider;
        _eventAggregator = eventAggregator;
        _dispatcherService = dispatcherService;
        _configuration = configuration;
        _logger = logger;
        _serviceName = _configuration["Connection:ServerDiscovery:ServiceName"];
        _networkInterface = _configuration["Connection:ServerDiscovery:NIC"];
        _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Subscribe(x => Messages.Insert(0, $"{x}"));
        _eventAggregator.GetEvent<PubSubEvent<Message<bool>>>().Subscribe(x => IsServerDataVisible = x.Payload);
        _eventAggregator.GetEvent<PubSubEvent<Message<ConnectionInfo>>>().Subscribe( x => ServerSelectedHandler(x.Payload));
        ServerContentViewModel = new ServerContentViewModel();
        IsServerDataVisible = false;
        SearchServers();
    }

    private void SearchServers()
    {
        if (_isSearching)
        {
            return;
        }

        _dispatcherService.Invoke(async() => {
            _isSearching = true;
            SearchMessage = SERVER_SEARCH_TEXT;
            await SearchServers(SEARCH_INTERVAL_IN_MS);

            Servers.Clear();
            foreach (var item in _connectionFeatureMap)
            {
                var vm = new ConnenctionInfoViewModel(item.Value.Item1, _eventAggregator, RegionManager);
            }
            Servers.AddRange(_connectionFeatureMap.Select(x => new ConnenctionInfoViewModel(x.Value.Item1, _eventAggregator, RegionManager)));
            SearchMessage = SERVER_SEARCH_FINISHED_TEXT;
            _isSearching = false;
        });
    }

    private void ServerSelectedHandler(ConnectionInfo connInfo)
    {
        ServerContentViewModel.IsVisible = false;
        ServerContentViewModel.ConnectionInfo = connInfo;
        if(_connectionFeatureMap.TryGetValue(connInfo.ToString(), out var value))
        {
            ServerContentViewModel.Features.Clear();
            ServerContentViewModel.Features.AddRange(value.Item2.Select(x =>
            {
                return new FeatureViewModel(x.Value, connInfo, _dynamicConfigurator, _commandPayloadProvider, _grpcChannelProvider, _eventAggregator);
            }));
        }
        ServerContentViewModel.IsVisible = true;
    }

    private async Task SearchServers(int duration)
    {
        try
        {
            Messages.Insert(0, new Message<string>(SERVER_SEARCH_TEXT).ToString());

            var connections = await _serviceFinder.GetConnections(_serviceName, _networkInterface, duration);

            foreach (ConnectionInfo connectionInfo in connections)
            {
                if (!_connectionFeatureMap.ContainsKey(connectionInfo.ToString()))
                {
                    GrpcChannel channel = await _dynamicConfigurator.GetChannel(connectionInfo.Address, connectionInfo.Port);
                    _connectionFeatureMap.AddOrUpdate(connectionInfo.ToString(),
                                                      key => (connectionInfo, new Dictionary<string, Feature>()), // Value factory for adding
                                                      (key, oldValue) => (connectionInfo, oldValue.Item2) // Update factory for existing
);
                    SiLAService.SiLAServiceClient serviceClient = new SiLAService.SiLAServiceClient(channel);
                    foreach (Sila2.Org.Silastandard.String implementedFeature in (await serviceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters())).ImplementedFeatures)
                    {
                        Feature value = FeatureGenerator.ReadFeatureFromXml(serviceClient.GetFeatureDefinition(new GetFeatureDefinition_Parameters
                        {
                            FeatureIdentifier = implementedFeature
                        }).FeatureDefinition.Value);
                        _connectionFeatureMap[connectionInfo.ToString()].Item2.Add(implementedFeature.Value, value);
                    }
                }
                Messages.Insert(0, new Message<string>($"Added Server {connectionInfo.ServerName} ({connectionInfo.Address}: {connectionInfo.Port})").ToString());
            }

            // Delete orphans
            var intersectKeys = _connectionFeatureMap.Keys.Intersect(connections.Select(x => x.ToString()));

            foreach (var key in _connectionFeatureMap.Keys.Except(intersectKeys))
            {
                
                if (_connectionFeatureMap.TryRemove(key, out var removedValue))
                {
                    // Successfully removed
                    Messages.Insert(0, new Message<string>($"Removed Orphan Server {_connectionFeatureMap[key].Item1.ServerName} ({_connectionFeatureMap[key].Item1.Address}: {_connectionFeatureMap[key].Item1.Port})").ToString());
                }
                else
                {
                    // Key was not found
                    Messages.Insert(0, new Message<string>($"Key not found: {key}").ToString());
                }
            }
        }
        catch (Exception ex)
        {
            Messages.Insert(0, new Message<string>(ex.ToString()).ToString());
        }
    }
}
