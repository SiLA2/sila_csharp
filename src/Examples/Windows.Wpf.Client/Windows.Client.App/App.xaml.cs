﻿using System.IO;
using System.Windows;

using DryIoc;

using MainModule;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Prism.DryIoc;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

using Serilog;

using SiLA2.Client.Dynamic;
using SiLA2.Utils.gRPC;

using Windows.Client.Services;
using Windows.Client.Shared;
using Windows.Client.Shared.Services;

namespace WindowsClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        private IDynamicConfigurator _dynamicClientConfigurator;

        protected override Window CreateShell()
        {
            return Container.Resolve<Windows.Client.App.Views.MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _dynamicClientConfigurator = new DynamicConfigurator(configuration, []);

            RegisterServiceCollection(_dynamicClientConfigurator.Container, containerRegistry.GetContainer());

            Log.Logger = new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .CreateLogger();

            containerRegistry.RegisterInstance(Log.Logger);
            containerRegistry.Register<ILoggerFactory, LoggerFactory>();
            containerRegistry.Register(typeof(ILogger<>), typeof(Logger<>));
            containerRegistry.RegisterSingleton<IConfiguration>(x => configuration);
            containerRegistry.RegisterSingleton<IDynamicConfigurator>(x => _dynamicClientConfigurator);
            containerRegistry.RegisterSingleton<IDispatcherService, DispatcherService>();
            containerRegistry.Register<ICommandPayloadProvider, CommandPayloadProvider>();
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<MainModuleModule>();
            base.ConfigureModuleCatalog(moduleCatalog);
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            Container.Resolve<IRegionManager>().RequestNavigate(RegionNames.CONTENT_REGION, ViewNames.MAIN_VIEW);
        }

        private void RegisterServiceCollection(IServiceCollection serviceCollection, IContainer container)
        {
            foreach (var service in serviceCollection)
            {
                var serviceType = service.ServiceType;
                var implementationType = service.ImplementationType;
                var implementationFactory = service.ImplementationFactory;
                var lifetime = service.Lifetime;

                switch (lifetime)
                {
                    case ServiceLifetime.Singleton:
                        if (implementationFactory != null)
                        {
                            container.RegisterDelegate(serviceType, implementationFactory, Reuse.Singleton);
                        }
                        else if (implementationType != null)
                        {
                            container.Register(serviceType, implementationType, Reuse.Singleton);
                        }
                        else
                        {
                            container.RegisterInstance(serviceType, service.ImplementationInstance);
                        }
                        break;

                    case ServiceLifetime.Scoped:
                        if (implementationFactory != null)
                        {
                            container.RegisterDelegate(serviceType, implementationFactory, Reuse.Scoped);
                        }
                        else if (implementationType != null)
                        {
                            container.Register(serviceType, implementationType, Reuse.Scoped);
                        }
                        break;

                    case ServiceLifetime.Transient:
                        if (implementationFactory != null)
                        {
                            container.RegisterDelegate(serviceType, implementationFactory, Reuse.Transient);
                        }
                        else if (implementationType != null)
                        {
                            container.Register(serviceType, implementationType, Reuse.Transient);
                        }
                        break;
                }
            }
        }
    }
}
