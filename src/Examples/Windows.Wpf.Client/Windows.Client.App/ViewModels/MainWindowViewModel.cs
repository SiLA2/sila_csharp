﻿using Prism.Mvvm;

namespace Windows.Client.App.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title;

        public string Title { get => _title; set => SetProperty(ref _title, value); }

        public MainWindowViewModel()
        {
            Title = "SiLA2 Client Windows App";
        }
    }
}
