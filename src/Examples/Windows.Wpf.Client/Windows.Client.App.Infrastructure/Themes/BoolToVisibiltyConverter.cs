﻿using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace Windows.Client.App.Infrastructure.Themes;

public class BoolToVisibilityConverter : IValueConverter
{
    public bool Invert { get; set; }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is bool boolValue)
        {
            if (Invert)
                boolValue = !boolValue;

            return boolValue ? Visibility.Visible : Visibility.Collapsed;
        }

        return DependencyProperty.UnsetValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is Visibility visibilityValue)
        {
            bool result = visibilityValue == Visibility.Visible;
            return Invert ? !result : result;
        }

        return DependencyProperty.UnsetValue;
    }
}
