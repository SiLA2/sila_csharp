﻿using System.Collections.ObjectModel;
using System.Windows.Input;

using Newtonsoft.Json;

using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;

using SiLA2;
using SiLA2.Client.Dynamic;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.gRPC;

using Windows.Client.Shared;
using Windows.Client.Shared.Services;

using static Google.Protobuf.Compiler.CodeGeneratorResponse.Types;

namespace Windows.Client.App.Infrastructure.ViewModels;

public class FeatureCommandViewModel : BindableBase
{
    private ICommand _cancelCommand;
    private ICommand _executeUnObservableCommand;
    private ICommand _executeObservableCommand;
    private readonly IGrpcChannelProvider _grpcChannelProvider;
    private readonly ICommandPayloadProvider _commandPayloadProvider;
    private readonly ConnectionInfo _connectionInfo;
    private readonly IEventAggregator _eventAggregator;
    private readonly IDynamicConfigurator _dynamicConfigurator;
    private CancellationTokenSource _cancellationTokenSource;
    private IDictionary<Tuple<string, string, string>, string> _parameterMap = new Dictionary<Tuple<string, string, string>, string>();
    private IDictionary<string, object> _payloadMap = new Dictionary<string, object>();

    public SiLA2.Feature Feature { get; }

    public FeatureCommand FeatureCommand { get; set; }
    public ObservableCollection<FeatureCommandParameterViewModel> CommandParameters { get; } = [];
    public string FeatureCommandDescription => FeatureCommand.Description.Replace("   ", " ");

    public ICommand CancelCommand => _cancelCommand ??= new DelegateCommand(() => _cancellationTokenSource?.Cancel());
    
    public ICommand ExecuteUnObservableCommand => _executeUnObservableCommand ??= new DelegateCommand<string>(async x =>
    {
        try
        {
            var channel = await _grpcChannelProvider.GetChannel(_connectionInfo.Address, _connectionInfo.Port, accceptAnyServerCertificate: true);
            _parameterMap = new Dictionary<Tuple<string, string, string>, string>();
            foreach(var parameter in CommandParameters)
            {
                _parameterMap.Add(new Tuple<string, string, string>(Feature.Identifier, FeatureCommand.Identifier, parameter.CommandParameter.Identifier), parameter.CommandParameterValue);
            }
            _payloadMap = _commandPayloadProvider.GetPayloadMap(_parameterMap, FeatureCommand, Feature);
            var response = _dynamicConfigurator.DynamicMessageService.ExecuteUnobservableCommand(x, channel, Feature, _payloadMap);
            var responseText = JsonConvert.SerializeObject(response, Formatting.Indented);
            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"Unobservable Command Request {Feature.Identifier}/{x} => {Environment.NewLine}{responseText}"));
        }
        catch (Exception ex)
        {
            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>(ex.ToString()));
        }
    });

    public ICommand ExecuteObservableCommand => _executeObservableCommand ??= new DelegateCommand<string>(x => { });

    public FeatureCommandViewModel(SiLA2.Feature feature,
                                  FeatureCommand featureCommand,
                                  IDynamicConfigurator dynamicConfigurator,
                                  IGrpcChannelProvider grpcChannelProvider,
                                  ICommandPayloadProvider commandPayloadProvider,
                                  ConnectionInfo connectionInfo,
                                  IEventAggregator eventAggregator)
    {
        _dynamicConfigurator = dynamicConfigurator;
        _grpcChannelProvider = grpcChannelProvider;
        _commandPayloadProvider = commandPayloadProvider;
        _connectionInfo = connectionInfo;
        _eventAggregator = eventAggregator;

        Feature = feature;
        FeatureCommand = featureCommand;

        SetupParameterMap();
    }

    private void SetupParameterMap()
    {
        if(FeatureCommand.Parameter == null)
        {
            return;
        }

        foreach (var parameter in FeatureCommand.Parameter)
        {
            //var cmdId = Feature.GetFullyQualifiedCommandParameterIdentifier(FeatureCommand.Identifier, parameter.Identifier);
            //_parameterMap.Add(new Tuple<string, string, string>())
            CommandParameters.Add(new FeatureCommandParameterViewModel(parameter));
        }
    }
}
