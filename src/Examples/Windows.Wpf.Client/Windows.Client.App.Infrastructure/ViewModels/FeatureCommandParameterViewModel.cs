﻿using Prism.Mvvm;

using SiLA2;

namespace Windows.Client.App.Infrastructure.ViewModels;

public class FeatureCommandParameterViewModel : BindableBase
{
    public SiLAElement CommandParameter { get; }

    public string ParameterType { get; }
    public string CommandParameterValue { get; set; }
    public string Description => CommandParameter.Description.Replace("   ", " ");

    public FeatureCommandParameterViewModel(SiLAElement commandParameter)
    {
        CommandParameter = commandParameter;
        ParameterType = GetParameterType(CommandParameter);
    }

    private string GetParameterType(SiLAElement commandParameter)
    {
        return commandParameter.DataType.Item.ToString() == "SiLA2.ConstrainedType" ? $"{commandParameter.DataType.Item}/{((ConstrainedType)(commandParameter.DataType.Item)).DataType.Item}" : $"{commandParameter.DataType.Item}";
    }
}
