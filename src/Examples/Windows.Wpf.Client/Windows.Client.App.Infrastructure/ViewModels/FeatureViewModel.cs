﻿using System.Collections.ObjectModel;

using Prism.Events;
using Prism.Mvvm;

using SiLA2;
using SiLA2.Client.Dynamic;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.gRPC;

using Windows.Client.Shared.Services;

namespace Windows.Client.App.Infrastructure.ViewModels;

public class FeatureViewModel : BindableBase
{
    private Feature _feature;
    private readonly ConnectionInfo _connectionInfo;
    private readonly IDynamicConfigurator _dynamicConfigurator;
    private readonly ICommandPayloadProvider _commandPayloadProvider;
    private readonly IGrpcChannelProvider _grpcChannelProvider;
    private readonly IEventAggregator _eventAggregator;

    public Feature Feature { get => _feature; set => SetProperty(ref _feature, value); }

    public string FeatureDescription => Feature.Description.Trim().Replace("   ", " ");

    public bool HasUnObservableProperties => UnObservableProperties.Count > 0;
    public bool HasObservableProperties => ObservableProperties.Count > 0;
    public bool HasUnObservableCommands => UnObservableCommands.Count > 0;
    public bool HasObservableCommands => ObservableCommands.Count > 0;

    public ObservableCollection<FeaturePropertyViewModel> UnObservableProperties { get; } = [];
    public ObservableCollection<FeaturePropertyViewModel> ObservableProperties { get; } = [];
    public ObservableCollection<FeatureCommandViewModel> UnObservableCommands { get; } = [];
    public ObservableCollection<FeatureCommandViewModel> ObservableCommands { get; } = [];

    public FeatureViewModel(Feature feature, ConnectionInfo connectionInfo, IDynamicConfigurator dynamicConfigurator, ICommandPayloadProvider commandPayloadProvider, IGrpcChannelProvider grpcChannelProvider, IEventAggregator eventAggregator)
    {
        _feature = feature;
        _connectionInfo = connectionInfo;
        _dynamicConfigurator = dynamicConfigurator;
        _commandPayloadProvider = commandPayloadProvider;
        _grpcChannelProvider = grpcChannelProvider;
        _eventAggregator = eventAggregator;
        Setup();
    }

    private void Setup()
    {
        var properties = Feature.GetDefinedProperties();
        UnObservableProperties.AddRange(properties.Where(x => FeaturePropertyObservable.No.Equals(x.Observable)).Select(x => new FeaturePropertyViewModel(_feature, x, _dynamicConfigurator, _grpcChannelProvider, _connectionInfo, _eventAggregator)));
        ObservableProperties.AddRange(properties.Where(x => FeaturePropertyObservable.Yes.Equals(x.Observable)).Select(x => new FeaturePropertyViewModel(_feature, x, _dynamicConfigurator, _grpcChannelProvider, _connectionInfo, _eventAggregator)));

        var commands = Feature.GetDefinedCommands();
        UnObservableCommands.AddRange(commands.Where(x => FeatureCommandObservable.No.Equals(x.Observable)).Select(x => new FeatureCommandViewModel(_feature, x, _dynamicConfigurator, _grpcChannelProvider, _commandPayloadProvider, _connectionInfo, _eventAggregator)));
        ObservableCommands.AddRange(commands.Where(x => FeatureCommandObservable.Yes.Equals(x.Observable)).Select(x => new FeatureCommandViewModel(_feature, x, _dynamicConfigurator, _grpcChannelProvider, _commandPayloadProvider, _connectionInfo, _eventAggregator)));
    }
}
