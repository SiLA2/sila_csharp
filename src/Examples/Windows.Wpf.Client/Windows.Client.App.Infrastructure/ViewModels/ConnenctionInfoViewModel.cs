﻿
using Prism.Commands;
using System.Windows.Input;

using Prism.Mvvm;

using SiLA2.Network.Discovery.mDNS;
using Prism.Events;
using Windows.Client.Shared;
using Prism.Regions;

namespace Windows.Client.App.Infrastructure.ViewModels;

public class ConnenctionInfoViewModel : BindableBase
{
    private ICommand _selectServerCommand;
    private ConnectionInfo _connenctionInfo;
    private readonly IEventAggregator _eventAggregator;
    private readonly IRegionManager _regionManager;

    public ConnectionInfo ConnectionInfo { get => _connenctionInfo; set => SetProperty(ref _connenctionInfo, value); }
    public string ServerName => ConnectionInfo.ServerName;

    public ICommand SelectServer => _selectServerCommand ??= new DelegateCommand<ConnenctionInfoViewModel>(x =>
    {
        _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"Selected Server {x.ServerName} at {x}"));
        _eventAggregator.GetEvent<PubSubEvent<Message<bool>>>().Publish(new Message<bool>(true));
        _eventAggregator.GetEvent<PubSubEvent<Message<ConnectionInfo>>>().Publish(new Message<ConnectionInfo>(ConnectionInfo));
    });

    public ConnenctionInfoViewModel(ConnectionInfo connenctionInfo, IEventAggregator eventAggregator, IRegionManager regionManager)
    {
        ConnectionInfo = connenctionInfo;
        _eventAggregator = eventAggregator;
        _regionManager = regionManager;
    }

    public override string ToString()
    {
        return ConnectionInfo.ToString();
    }
}
