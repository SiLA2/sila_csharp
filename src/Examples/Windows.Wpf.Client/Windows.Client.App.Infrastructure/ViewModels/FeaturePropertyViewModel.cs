﻿using System.Windows.Input;

using Grpc.Core;

using Newtonsoft.Json;

using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;

using SiLA2;
using SiLA2.Client.Dynamic;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.gRPC;
using Windows.Client.Shared;

using static Google.Protobuf.Compiler.CodeGeneratorResponse.Types;

namespace Windows.Client.App.Infrastructure.ViewModels;

public class FeaturePropertyViewModel : BindableBase
{
    private ICommand _cancelCommand;
    private ICommand _queryUnObservablePropertyCommand;
    private ICommand _queryObservablePropertyCommand;
    private readonly IDynamicConfigurator _dynamicConfigurator;
    private readonly IGrpcChannelProvider _grpcChannelProvider;
    private readonly ConnectionInfo _connectionInfo;
    private readonly IEventAggregator _eventAggregator;
    private CancellationTokenSource _cancellationTokenSource;

    public SiLA2.Feature Feature { get; }
    public FeatureProperty FeatureProperty { get; set; }

    public string FeaturePropertyDescription => FeatureProperty.Description.Replace("   ", " ");

    public ICommand CancelCommand => _cancelCommand ??= new DelegateCommand(() => _cancellationTokenSource?.Cancel());

    public ICommand QueryUnObservableProperty => _queryUnObservablePropertyCommand ??= new DelegateCommand<object>(async x =>
    {
        try
        {
            var channel = await _grpcChannelProvider.GetChannel(_connectionInfo.Address, _connectionInfo.Port, accceptAnyServerCertificate: true);
            var response = _dynamicConfigurator.DynamicMessageService.GetUnobservableProperty(x.ToString(), channel, Feature);
            var responseText = JsonConvert.SerializeObject(response, Formatting.Indented);
            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"Unobservable Property Request {Feature.Identifier}/{x} => {Environment.NewLine}{responseText}"));
        }
        catch (Exception ex)
        {
            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>(ex.ToString()));
        }
    });

    public ICommand QueryObservableProperty => _queryObservablePropertyCommand ??= new DelegateCommand<object>(async x =>
    {
        try
        {
            var channel = await _grpcChannelProvider.GetChannel(_connectionInfo.Address, _connectionInfo.Port, accceptAnyServerCertificate: true);
            var asyncEnumerable = _dynamicConfigurator.DynamicMessageService.SubcribeObservableProperty(x.ToString(), channel, Feature);

            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"Observable Property Request {Feature.Identifier}/{x} => {Environment.NewLine}"));
            _cancellationTokenSource?.Cancel();
            _cancellationTokenSource = new CancellationTokenSource();
            var enumerator = asyncEnumerable.GetAsyncEnumerator(_cancellationTokenSource.Token);
            while (await enumerator.MoveNextAsync())
            {
                _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"{JsonConvert.SerializeObject(enumerator.Current, Formatting.Indented)}{Environment.NewLine}"));
            }
        }
        catch(RpcException rpcEx)
        {
            if (StatusCode.Cancelled.Equals(rpcEx.StatusCode))
            {
                _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>($"Observable Property Request {Feature.Identifier}/{x} was cancelled."));
            }
        }
        catch (Exception ex)
        {
            _eventAggregator.GetEvent<PubSubEvent<Message<string>>>().Publish(new Message<string>(ex.ToString()));
        }
    });

    public FeaturePropertyViewModel(SiLA2.Feature feature, FeatureProperty featureProperty, IDynamicConfigurator dynamicConfigurator, IGrpcChannelProvider grpcChannelProvider, ConnectionInfo connectionInfo, IEventAggregator eventAggregator)
    {
        Feature = feature;
        FeatureProperty = featureProperty;
        _dynamicConfigurator = dynamicConfigurator;
        _grpcChannelProvider = grpcChannelProvider;
        _connectionInfo = connectionInfo;
        _eventAggregator = eventAggregator;
    }
}
