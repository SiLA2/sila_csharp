﻿using Prism.Mvvm;
using Prism.Navigation;

namespace Windows.Client.App.Infrastructure;

public abstract class ViewModelBase : BindableBase, IDestructible
{
    protected ViewModelBase()
    {

    }

    public virtual void Destroy()
    {

    }
}
