﻿using Sila2.Org.Silastandard.Core.Authenticationservice.V1;
using SiLA2.Server.Services;
using System.Threading.Tasks;

namespace TemperatureController.Features.Services
{
    public class DummyAuthenticationInspector : IAuthenticationInspector
    {
        public Task<bool> IsAuthenticated(Login_Parameters login)
        {
            return Task.FromResult(true);
        }
    }
}
