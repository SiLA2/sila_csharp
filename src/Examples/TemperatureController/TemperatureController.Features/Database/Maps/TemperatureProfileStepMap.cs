﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TemperatureController.Features.Database.Maps
{
    public class TemperatureProfileStepMap : IEntityTypeConfiguration<TemperatureProfileStep>
    {
        public void Configure(EntityTypeBuilder<TemperatureProfileStep> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.TemperatureInCelsius);
            builder.Property(p => p.DurationInMilliseconds);
            builder.ToTable("TemperatureProfileSteps");
        }
    }
}
