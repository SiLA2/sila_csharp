﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TemperatureController.Features.Database.Maps
{
    public class TemperatureProfileMap : IEntityTypeConfiguration<TemperatureProfile>
    {
        public void Configure(EntityTypeBuilder<TemperatureProfile> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.ClockInMilliseconds);
            builder.Property(p => p.Loops);
            builder.HasMany(p => p.Temperatures);
            builder.ToTable("TemperatureProfiles");
        }
    }
}
