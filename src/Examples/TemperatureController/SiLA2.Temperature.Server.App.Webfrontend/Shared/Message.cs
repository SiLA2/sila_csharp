﻿namespace SiLA2.Temperature.Server.App.Webfrontend.Shared
{
    public class FileSystemItem
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public bool IsDownloading { get; set; }

        public override string ToString()
        {
            return $"{Type} : {Name} ({Size} Bytes)";
        }
    }
}
