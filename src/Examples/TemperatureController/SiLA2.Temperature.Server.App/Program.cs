using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.AspNetCore;
using SiLA2.Commands;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Network.Discovery;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Simulation;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using System.IO;
using System.Reflection;
using System;
using TemperatureController.Features.Services;
using static Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController;
using Serilog;
using Microsoft.Extensions.Hosting;

namespace SiLA2.Temperature.Server.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder();

            ConfigureServices(builder.Services, builder.Configuration);
            builder.WebHost.ConfigureKestrel(serverOptions =>
            {
                var kestrelServerConfigData = args.GetKestrelConfigData(serverOptions.ApplicationServices);
                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                serverOptions.Listen(kestrelServerConfigData.Item1, kestrelServerConfigData.Item2, listenOptions => listenOptions.UseHttps(kestrelServerConfigData.Item3));
            });
            builder.Host.UseSerilog((context, configuration) =>
                configuration.ReadFrom.Configuration(context.Configuration));

#if DEBUG
            builder.AddServiceDefaults();
#endif

            var app = builder.Build();
            ConfigureApplication(app);

            app.Run();
        }

        private static void ConfigureApplication(WebApplication app)
        {
            var env = app.Services.GetService<IWebHostEnvironment>();
            var siLA2Server = app.Services.GetService<ISiLA2Server>();
            var logger = app.Services.GetService<ILogger<Program>>();

            app.InitializeSiLA2Features(siLA2Server);

            app.MapGrpcService<SiLAService>();
            app.MapGrpcService<LockControllerService>();
            app.MapGrpcService<SiLABinaryDownloadService>();
            app.MapGrpcService<SiLABinaryUploadService>();
            app.MapGrpcService<TemperatureControllerService>();
            app.MapGrpcService<FileTransferService>();
            app.MapGrpcService<AuthenticationService>();
            app.MapGrpcService<AuthorizationService>();
            
            app.MapGrpcReflectionService();

            logger.LogInformation($"{siLA2Server.ServerInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<SiLA2.Server.Interceptors.LoggingInterceptor>();
                options.Interceptors.Add<SiLA2.Server.Interceptors.MetadataValidationInterceptor>();
                options.Interceptors.Add<SiLA2.Server.Interceptors.ParameterValidationInterceptor>();
            });
            services.AddSingleton<MetadataManager>();
            services.AddGrpcReflection();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<IThermostatSimulator, ThermostatSimulator>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<TemperatureControllerService>();
            services.AddSingleton<FileTransferService>();
            services.AddSingleton<SiLABinaryUploadService>();
            services.AddSingleton<IBinaryUploadRepository, BinaryUploadRepository>();
            services.AddSingleton<IBinaryDownloadRepository, BinaryDownloadRepository>();
            services.AddScoped<IAuthenticationInspector, DummyAuthenticationInspector>();
            services.AddSingleton<AuthorizationService>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<ICertificateContext, CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<ErrorRecoveryServiceImpl>();
            ServerConfig serverConfig = new ServerConfig(configuration["ServerConfig:Name"],
                                                         Guid.Parse(configuration["ServerConfig:UUID"]),
                                                         configuration["ServerConfig:FQHN"],
                                                         int.Parse(configuration["ServerConfig:Port"]),
                                                         configuration["ServerConfig:NetworkInterface"],
                                                         configuration["ServerConfig:DiscoveryServiceName"]);
            services.AddSingleton<IServerConfig>(serverConfig);
            services.AddSingleton(x =>
            {
                var channel = x.GetRequiredService<IGrpcChannelProvider>().GetChannel(serverConfig.FQHN, serverConfig.Port, true);
                return new TemperatureControllerClient(channel.Result);
            });
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(configuration.GetSection("ServerConfig"), configFile);
        }
    }
}
