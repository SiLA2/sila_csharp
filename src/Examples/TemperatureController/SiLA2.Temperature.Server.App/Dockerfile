FROM mcr.microsoft.com/dotnet/aspnet:9.0 AS base
WORKDIR /app
EXPOSE 13742

FROM mcr.microsoft.com/dotnet/sdk:9.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["Examples/TemperatureController/SiLA2.Temperature.Server.App/SiLA2.Temperature.Server.App.csproj", "Examples/TemperatureController/SiLA2.Temperature.Server.App/"]
COPY ["SiLA2.AnIML/SiLA2.AnIML.csproj", "SiLA2.AnIML/"]
COPY ["SiLA2.Database.NoSQL/SiLA2.Database.NoSQL.csproj", "SiLA2.Database.NoSQL/"]
COPY ["SiLA2.AspNetCore/SiLA2.AspNetCore.csproj", "SiLA2.AspNetCore/"]
COPY ["SiLA2/SiLA2.csproj", "SiLA2/"]
COPY ["SiLA2.Utils/SiLA2.Utils.csproj", "SiLA2.Utils/"]
COPY ["Examples/TemperatureController/TemperatureController.Features/TemperatureController.Features.csproj", "Examples/TemperatureController/TemperatureController.Features/"]
COPY ["SiLA2.Database.SQL/SiLA2.Database.SQL.csproj", "SiLA2.Database.SQL/"]
COPY ["Examples/SiLA2.Simulation/SiLA2.Simulation.csproj", "Examples/SiLA2.Simulation/"]
RUN dotnet restore "./Examples/TemperatureController/SiLA2.Temperature.Server.App/./SiLA2.Temperature.Server.App.csproj"
COPY . .
WORKDIR "/src/Examples/TemperatureController/SiLA2.Temperature.Server.App"
RUN dotnet build "./SiLA2.Temperature.Server.App.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./SiLA2.Temperature.Server.App.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SiLA2.Temperature.Server.App.dll"]