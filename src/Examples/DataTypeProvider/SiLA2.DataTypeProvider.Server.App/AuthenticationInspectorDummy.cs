﻿using Sila2.Org.Silastandard.Core.Authenticationservice.V1;
using SiLA2.Server.Services;

namespace SiLA2.DataTypeProvider.Server.App
{
    internal class AuthenticationInspectorDummy : IAuthenticationInspector
    {
        public Task<bool> IsAuthenticated(Login_Parameters login)
        {
            return Task.FromResult(true);
        }
    }
}