﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SiLA2.Client;
using SiLA2.Server.Utils;
using SiLA2.Shaker.Client;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.Network;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Shaker.Service.Client.App
{
    class Program
    {
        private static IConfigurationRoot _configuration;
        //private static ILogger _logger; 

        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            _configuration = configBuilder.Build();

            var clientSetup = new Configurator(_configuration, args);
            //ILoggerFactory loggerFactory = SetupConsoleLogger(clientSetup);

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(_configuration)
                    .CreateLogger();

            clientSetup.Container.AddLogging(x => {
                x.ClearProviders();
                x.AddSerilog(dispose: true);
            });
            clientSetup.UpdateServiceProvider();

            var _logger = clientSetup.ServiceProvider.GetRequiredService<ILogger<Program>>();
            _logger.LogInformation("Starting Server Discovery...");

            var serverMap = await clientSetup.SearchForServers();

            GrpcChannel channel;
            var serverType = "SiLA2ShakerServer";
            var server = serverMap.Values.FirstOrDefault(x => x.ServerType == serverType);
            if (server != null)
            {
                _logger.LogInformation($"{Environment.NewLine}Found Server");
                _logger.LogInformation(server.ServerInfo);
                _logger.LogInformation($"Connecting to {server}");
                channel = await clientSetup.GetChannel(server.Address, server.Port, acceptAnyServerCertificate: false, server.SilaCA.GetCaFromFormattedCa());
            }
            else
            {
                var clientConfig = clientSetup.ServiceProvider.GetService<IClientConfig>();
                _logger.LogWarning($"No connection automatically discovered. Using Server-URI '{clientConfig.IpOrCdirOrFullyQualifiedHostName}:{clientConfig.Port}' from ClientConfig");
                channel = await clientSetup.GetChannel(acceptAnyServerCertificate: true);
            }

            try
            {
                _logger.LogInformation("Trying to setup Client ...");
                // create the client
                var client = new ShakerControllerClientImpl(channel, clientSetup.ServiceProvider.GetService<ILoggerFactory>());

                // test clamp handling
                DisplayClampState(client);
                client.OpenClamp();
                DisplayClampState(client);

                // try shaking with invalid duration parameter
                try
                {
                    await client.Shake("7S", 3000);
                }
                catch (Exception e)
                {
                    _logger.LogError(ErrorHandling.HandleException(e));
                }

                // try shaking with opened clamp
                try
                {
                    await client.Shake("PT7S", 3000);
                }
                catch (Exception e)
                {
                    _logger.LogError(ErrorHandling.HandleException(e));
                }

                // close clamp in order to shake
                client.CloseClamp();
                DisplayClampState(client);

                // shake for 7 seconds
                await client.Shake("PT7S", 3000);

                _logger.LogInformation("Shutting down connection...");

                await channel.ShutdownAsync();
                Console.WriteLine();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");
            }

            Console.ReadKey();
        }

        //private static ILoggerFactory SetupConsoleLogger(Configurator clientSetup)
        //{
        //    clientSetup.Container.AddLogging(x => x.AddConsole());
        //    clientSetup.UpdateServiceProvider();
        //    var loggerFactory = clientSetup.ServiceProvider.GetService<ILoggerFactory>();
        //    _logger = loggerFactory.CreateLogger<Program>();
        //    return loggerFactory;
        //}

        private static void DisplayClampState(ShakerControllerClientImpl client)
        {
            var clampState = client.IsClampOpen();
            if (clampState.HasValue)
            {
                Console.WriteLine($"Clamp state = {(clampState.Value ? "open" : "closed")}");
            }
            else
            {
                Console.WriteLine("Clamp state is undefined");
            }
        }
    }
}
