﻿using NUnit.Framework;
using SiLA2.Utils;

namespace SiLA2.Server.Tests
{
    [TestFixture]
    public class PasswordHashServiceTests
    {
        [Test]
        public void ShouldVerfifyExistingPassword()
        {
            //Arrange
            const string PWD = "T3st!";

            //SUT
            var resultPasswordHash = new PasswordHashService();

            //Act
            var hashedPwd = resultPasswordHash.Hash(PWD);
            var result = resultPasswordHash.Verify(hashedPwd, PWD);

            //Assert
            Assert.That(result, Is.True);
        }
    }
}