﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Sila2.Org.Silastandard;
using SiLA2.Client.Dynamic;
using SiLA2.Network.Discovery.mDNS;

namespace SiLA2.IntegrationTests.DynamicClientApp
{
    internal class Program
    {
        internal static readonly ConnectionInfo _connectionInfo = new ConnectionInfo("127.0.0.1", 50052);

        static async Task Main(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            var client = new DynamicConfigurator(configuration, args);
            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .CreateLogger();

            client.Container.AddLogging(x => {
                x.ClearProviders();
                x.AddSerilog(dispose: true);
            });
            client.UpdateServiceProvider();

            var logger = client.ServiceProvider.GetRequiredService<ILogger<Program>>();

            // The ConnectionInfo is provided for SiLA2.IntegrationTests.ServerApp connection.
            // It should not be necessary...call 'await client.GetFeatures()' for Server Discovery instead...
            _ = await client.GetFeatures([_connectionInfo]);

            int i = 0;
            do
            {
                try
                {
                    i = 1;
                    var list = new List<Tuple<string, Feature>>();
                    Console.WriteLine($"{Environment.NewLine}Following Features were found by Dynamic Client Search:{Environment.NewLine}");
                    foreach (var server in client.ServerFeatureMap)
                    {
                        foreach (var feature in server.Value)
                        {
                            Console.WriteLine($"\t{i}\t\t{server.Key.Address}:{server.Key.Port}\t{feature.Key}");
                            list.Add(new Tuple<string, Feature>(feature.Key, feature.Value));
                            i++;
                        }
                    }

                    Console.WriteLine($"{Environment.NewLine}Enter number of Feature (or hit just Enter to exit):");

                    var input = Console.ReadLine();
                    if(!int.TryParse(input, out int ix))
                    {
                        break;
                    }
                    else
                    {
                        i = ix;
                    }

                    if (i > 0 && ix <= list.Count)
                    {
                        var feature = list[ix - 1].Item2;
                        Console.WriteLine(feature);
                    }

                    Console.WriteLine($"{Environment.NewLine}Press any key to continue...");
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            } while (i > 0);

            if (client.ServerFeatureMap.Count > 0)
            {
                var channel = client.GetChannel(_connectionInfo.Address, _connectionInfo.Port).Result;
                CallUnobservableProperty(client, channel);
                CallUnobservableCommand(client, channel);
                await CallObservableProperty(client, channel);
                await CallObservableCommand(client, channel);
            }
            else
            {
                Console.WriteLine("No Server found...please restart App or provide correct server connection data in GetFeatures Method...");
            }


            Console.WriteLine($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();
        }

        private static void CallUnobservableProperty(IDynamicConfigurator client, GrpcChannel channel)
        {
            Console.WriteLine($"{Environment.NewLine}Calling unobservable property to get Server UUID...");
            var silaServiceFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/core/SiLAService/v1");
            dynamic serverUUIDResponse = client.DynamicMessageService.GetUnobservableProperty("ServerUUID", channel, silaServiceFeature);
            Console.WriteLine($"Response => ServerUUID: {serverUUIDResponse.ServerUUID.Value}");
        }

        private static void CallUnobservableCommand(IDynamicConfigurator client, GrpcChannel channel)
        {
            const string COMMAND_IDENTIFIER = "JoinIntegerAndString";
            Console.WriteLine($"{Environment.NewLine}Calling Unobservable Command '{COMMAND_IDENTIFIER}' with Integer and String Parameter...");

            int intVal = int.MinValue;
            do
            {
                Console.WriteLine("Enter Integer");
                string tmp = Console.ReadLine();
                if (int.TryParse(tmp, out int intValTmp))
                {
                    intVal = intValTmp;
                    break;
                }
            } while (true);
            Console.WriteLine("Enter String");
            string strVal = Console.ReadLine();

            var unobservableCommandFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/UnobservableCommandTest/v1");

            Dictionary<string, object> payloadMap = new Dictionary<string, object> {
                    { "Integer", new Sila2.Org.Silastandard.Protobuf.Integer { Value = intVal } },
                    { "String", new Sila2.Org.Silastandard.Protobuf.String { Value = strVal } }
                };

            dynamic joinIntAndStringResponse = client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, channel, unobservableCommandFeature, payloadMap);
            Console.WriteLine($"Response => JoinedParameters: {joinIntAndStringResponse.JoinedParameters.Value}");
        }

        private async static Task CallObservableProperty(IDynamicConfigurator client, GrpcChannel channel)
        {
            const string PROPERTY_NAME = "Alternating";
            Console.WriteLine($"{Environment.NewLine}Calling Observable Property returning alternating Boolean Value for 7 seconds...");
            var silaObservablePropertyFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/ObservablePropertyTest/v1");
            var response = client.DynamicMessageService.SubcribeObservableProperty(PROPERTY_NAME, channel, silaObservablePropertyFeature);

            var asyncEnumerator = response.GetAsyncEnumerator();

            int i = 0;
            while (await asyncEnumerator.MoveNextAsync() && i <= 7)
            {
                dynamic responseResultPropertyValue = asyncEnumerator.Current;
                Console.WriteLine($"Response => {responseResultPropertyValue.Alternating.Value}");
                i++;
            }
        }

        private static async Task CallObservableCommand(DynamicConfigurator client, GrpcChannel channel)
        {
            const string OPERATION_NAME = "Count";
            const double DELAY = 1.0;
            Console.WriteLine($"{Environment.NewLine}Calling Observable Command for 7 seconds with delay of 1 second...");

            var observableCommandFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/ObservableCommandTest/v1");

            IDictionary<string, object> payloadMap = new Dictionary<string, object> {
                { "N", new Sila2.Org.Silastandard.Protobuf.Integer { Value = 7 } },
                { "Delay", new Sila2.Org.Silastandard.Protobuf.Real { Value = DELAY } }
            };

            var response = client.DynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, channel, observableCommandFeature, payloadMap);

            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            while (await asyncEnumerator.MoveNextAsync())
            {
                Console.WriteLine($"Response => {asyncEnumerator.Current.CommandStatus}");
                if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }

            dynamic result = client.DynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, channel, observableCommandFeature, response.Item4);
            Console.WriteLine($"Result Response => Number of Iterations => {result.IterationResponse.Value}");
        }
    }
}