using SiLA2.Utils.Network;
using SiLA2.Utils.Security;
using SiLA2.Commands;
using SiLA2.IntegrationTests.Features.Service.Implementations;
using SiLA2.IntegrationTests.Features.ServiceImplementations;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Network.Discovery;
using SiLA2.Server.Services;
using SiLA2.Server;
using SiLA2.Utils.Config;
using SiLA2.Utils.gRPC;
using System.Reflection;
using SiLA2.AspNetCore;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;

namespace SiLA2.IntegrationTests.ServerApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder();
            ConfigureServices(builder.Services, builder.Configuration);
            builder.WebHost.ConfigureKestrel(serverOptions =>
            {
                var kestrelServerConfigData = args.GetKestrelConfigData(serverOptions.ApplicationServices);
                serverOptions.ConfigureEndpointDefaults(endpoints => endpoints.Protocols = HttpProtocols.Http1AndHttp2);
                serverOptions.Listen(kestrelServerConfigData.Item1, kestrelServerConfigData.Item2, listenOptions => listenOptions.UseHttps(kestrelServerConfigData.Item3));
            });

            builder.Host.UseSerilog((context, configuration) =>
                configuration.ReadFrom.Configuration(context.Configuration));

#if DEBUG
            builder.AddServiceDefaults();
#endif

            var app = builder.Build();
            ConfigureApplication(app);

            app.Run();
        }

        private static void ConfigureApplication(WebApplication app)
        {
            var env = app.Services.GetService<IWebHostEnvironment>();
            var siLA2Server = app.Services.GetService<ISiLA2Server>();
            var logger = app.Services.GetService<ILogger<Program>>();

            app.InitializeSiLA2Features(siLA2Server);

            app.MapGrpcService<SiLAService>();
            app.MapGrpcService<AuthenticationService>();
            app.MapGrpcService<AuthorizationService>();
            app.MapGrpcService<SiLABinaryUploadService>();
            app.MapGrpcService<SiLABinaryDownloadService>();
            app.MapGrpcService<LockControllerService>();
            app.MapGrpcService<AnyTypeTestServiceImpl>();
            app.MapGrpcService<BasicDataTypeTestServiceImpl>();
            app.MapGrpcService<BinaryTransferTestServiceImpl>();
            app.MapGrpcService<BinaryDownloadRepository>();
            app.MapGrpcService<BinaryUploadRepository>();
            app.MapGrpcService<ErrorHandlingTestServiceImpl>();
            app.MapGrpcService<ListDataTypeTestServiceImpl>();
            app.MapGrpcService<LockableCommandProviderServiceImpl>();
            app.MapGrpcService<MetadataConsumerTestServiceImpl>();
            app.MapGrpcService<MetadataProviderServiceImpl>();
            app.MapGrpcService<MultiClientTestServiceImpl>();
            app.MapGrpcService<ParameterConstraintsTestServiceImpl>();
            app.MapGrpcService<StructureDataTypeTestServiceImpl>();
            app.MapGrpcService<UnobservableCommandTestServiceImpl>();
            app.MapGrpcService<UnobservablePropertyTestServiceImpl>();
            app.MapGrpcService<ObservableCommandTestServiceImpl>();
            app.MapGrpcService<ObservablePropertyTestServiceImpl>();
            app.MapGrpcService<AuthenticationTestImpl>();

            app.MapGet("/", async context => await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909"));

            logger.LogInformation($"{siLA2Server.ServerInformation}");
            logger.LogInformation("Starting Server Announcement...");
            siLA2Server.Start();
            logger.LogInformation($"{DateTime.Now} : Started {Assembly.GetExecutingAssembly().FullName}");
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.Interceptors.Add<Server.Interceptors.LoggingInterceptor>();
                options.Interceptors.Add<Server.Interceptors.MetadataValidationInterceptor>();
                options.Interceptors.Add<Server.Interceptors.ParameterValidationInterceptor>();
            });
            services.AddSingleton(typeof(IObservableCommandManager<,>), typeof(ObservableCommandManager<,>));
            services.AddTransient<INetworkService, NetworkService>();
            services.AddSingleton<ServiceDiscoveryInfo>();
            services.AddSingleton<ServerInformation>();
            services.AddTransient<IServiceAnnouncer, ServiceAnnouncer>();
            services.AddSingleton<MetadataManager>();
            services.AddSingleton<ISiLA2Server, SiLA2Server>();
            services.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            services.AddScoped<IServerDataProvider, ServerDataProvider>();
            services.AddSingleton<ICertificateProvider, CertificateProvider>();
            services.AddSingleton<ICertificateContext, CertificateContext>();
            services.AddSingleton<ICertificateRepository, CertificateRepository>();
            services.AddSingleton<IBinaryDownloadRepository, BinaryDownloadRepository>();
            services.AddSingleton<IBinaryUploadRepository, BinaryUploadRepository>();
            services.AddSingleton<AuthenticationService>();
            services.AddSingleton<IAuthenticationInspector, AuthenticationInspectorTestImpl>();
            services.AddSingleton<AuthorizationService>();
            services.AddSingleton<AuthenticationTestImpl>();
            services.AddSingleton<LockControllerService>();
            services.AddSingleton<AnyTypeTestServiceImpl>();
            services.AddSingleton<BasicDataTypeTestServiceImpl>();
            services.AddSingleton<BinaryTransferTestServiceImpl>();
            services.AddSingleton<ErrorHandlingTestServiceImpl>();
            services.AddSingleton<ListDataTypeTestServiceImpl>();
            services.AddSingleton<LockableCommandProviderServiceImpl>();
            services.AddSingleton<MetadataConsumerTestServiceImpl>();
            services.AddSingleton<MetadataProviderServiceImpl>();
            services.AddSingleton<ParameterConstraintsTestServiceImpl>();
            services.AddSingleton<MultiClientTestServiceImpl>();
            services.AddSingleton<StructureDataTypeTestServiceImpl>();
            services.AddSingleton<UnobservableCommandTestServiceImpl>();
            services.AddSingleton<UnobservablePropertyTestServiceImpl>();
            services.AddSingleton<ObservableCommandTestServiceImpl>();
            services.AddSingleton<ObservablePropertyTestServiceImpl>();
            services.AddSingleton<IServerConfig>(new ServerConfig(configuration["ServerConfig:Name"],
                                                                  Guid.Parse(configuration["ServerConfig:UUID"]),
                                                                  configuration["ServerConfig:FQHN"],
                                                                  int.Parse(configuration["ServerConfig:Port"]),
                                                                  configuration["ServerConfig:NetworkInterface"],
                                                                  configuration["ServerConfig:DiscoveryServiceName"]));
#if DEBUG
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.Development.json");
#else
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "appsettings.json");
#endif
            services.ConfigureWritable<ServerConfig>(configuration.GetSection("ServerConfig"), configFile);
        }
    }
}
