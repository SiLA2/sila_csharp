﻿using Sila2.Org.Silastandard;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class MultiClientTests
    {
        private IList<IDynamicConfigurator> _clients;
        private Feature _silaFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _clients = await ClientBootstrapper.SetupClients(2);
            _silaFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "MultiClientTest-v1_0.sila.xml"));
        }

        [Test]
        public async Task Queued_Execution_Should_Return_Correct_ExecutionInfos()
        {
            // Arrange
            const string OPERATION_NAME = "RunQueued";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "Duration", new SiLAFramework.Protobuf.Real { Value = 1 } } };

            // Systems under Test & Act
            List<Tuple<DateTime,ExecutionInfo>> executionInfosQueuedCommandExecutions1 = new();
            List<Tuple<DateTime, ExecutionInfo>> executionInfosQueuedCommandExecutions2 = new();
            await GetExecutionInfos(_clients[0], OPERATION_NAME, payloadMap, executionInfosQueuedCommandExecutions1);
            await GetExecutionInfos(_clients[1], OPERATION_NAME, payloadMap, executionInfosQueuedCommandExecutions2);

            // Assert
            Assert.That(executionInfosQueuedCommandExecutions1[0].Item2.CommandStatus, Is.EqualTo(ExecutionInfo.Types.CommandStatus.Running));
            Assert.That(executionInfosQueuedCommandExecutions1[1].Item2.CommandStatus, Is.EqualTo(ExecutionInfo.Types.CommandStatus.FinishedSuccessfully));
            Assert.That(executionInfosQueuedCommandExecutions2[0].Item2.CommandStatus, Is.EqualTo(ExecutionInfo.Types.CommandStatus.Running));
            Assert.That(executionInfosQueuedCommandExecutions2[1].Item2.CommandStatus, Is.EqualTo(ExecutionInfo.Types.CommandStatus.FinishedSuccessfully));
            Assert.That(executionInfosQueuedCommandExecutions1[0].Item1 < executionInfosQueuedCommandExecutions1[1].Item1);
            Assert.That(executionInfosQueuedCommandExecutions1[1].Item1 < executionInfosQueuedCommandExecutions2[0].Item1);
            Assert.That(executionInfosQueuedCommandExecutions2[0].Item1 < executionInfosQueuedCommandExecutions2[1].Item1);
        }

        private async Task GetExecutionInfos(IDynamicConfigurator client, string OPERATION_NAME, IDictionary<string, object> payloadMap, List<Tuple<DateTime, ExecutionInfo>> list1)
        {
            var response = client.DynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, await client.GetChannel(), _silaFeature, payloadMap);
            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            while (await asyncEnumerator.MoveNextAsync())
            {
                list1.Add(new Tuple<DateTime, ExecutionInfo>(DateTime.Now, asyncEnumerator.Current));
                if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }
        }
    }
}
