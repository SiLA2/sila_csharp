﻿using Google.Protobuf;

using Newtonsoft.Json;

using Sila2.Org.Silastandard.Protobuf;
using Sila2.Org.Silastandard.Test.Structuredatatypetest.V1;

using SiLA2.Communication.Services;
using SiLA2.Server.Utils;

using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class DynamicClientStructureDataTypeTests
    {
        private GrpcChannel _channel;
        private Feature _silaStructureDataTypeFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaStructureDataTypeFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "StructureDataTypeTest-v1_0.sila.xml"));
        }


        [Test]
        public void Should_Get_Structure_Property()
        {
            // Arrange
            const string PROPERTY_NAME = "StructureValue";

            const string EXPECTED_STRING_TYPE_VALUE = "SiLA2_Test_String_Value";
            const long EXPECTED_INTEGER_TYPE_VALUE = 5124;
            const double EXPECTED_REAL_TYPE_VALUE = 3.1415926;
            const bool EXPECTED_BOOLEAN_TYPE_VALUE = true;
            SiLAFramework.Binary EXPECTED_BINARY_TYPE_VALUE = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") };
            const int EXPECTED_DATE_DAY_VALUE = 5;
            const int EXPECTED_DATE_MONTH_VALUE = 8;
            const int EXPECTED_DATE_YEAR_VALUE = 2022;
            const int EXPECTED_TIME_HOUR_VALUE = 12;
            const int EXPECTED_TIME_MINUTE_VALUE = 34;
            const int EXPECTED_TIME_SECOND_VALUE = 56;
            const int EXPECTED_TIME_MILLISECOND_VALUE = 789;
            SiLAFramework.Any EXPECTED_ANY_TYPE_VALUE = AnyType.CreateAnyTypeObject("SiLA2_Any_Type_String_Value");

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            dynamic response = dynamicMessageService.GetUnobservableProperty(PROPERTY_NAME, _channel, _silaStructureDataTypeFeature);

            // Assert
            Assert.That(response.StructureValue.TestStructure.StringTypeValue.Value, Is.EqualTo(EXPECTED_STRING_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.IntegerTypeValue.Value, Is.EqualTo(EXPECTED_INTEGER_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.RealTypeValue.Value, Is.EqualTo(EXPECTED_REAL_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.BooleanTypeValue.Value, Is.EqualTo(EXPECTED_BOOLEAN_TYPE_VALUE));
            Assert.That(response.StructureValue.TestStructure.BinaryTypeValue.Value, Is.EqualTo(EXPECTED_BINARY_TYPE_VALUE.Value.Span.ToArray()));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Year, Is.EqualTo(EXPECTED_DATE_YEAR_VALUE));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Month, Is.EqualTo(EXPECTED_DATE_MONTH_VALUE));
            Assert.That(response.StructureValue.TestStructure.DateTypeValue.Day, Is.EqualTo(EXPECTED_DATE_DAY_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Hour, Is.EqualTo(EXPECTED_TIME_HOUR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Minute, Is.EqualTo(EXPECTED_TIME_MINUTE_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Second, Is.EqualTo(EXPECTED_TIME_SECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimeTypeValue.Millisecond, Is.EqualTo(EXPECTED_TIME_MILLISECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Year, Is.EqualTo(EXPECTED_DATE_YEAR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Month, Is.EqualTo(EXPECTED_DATE_MONTH_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Day, Is.EqualTo(EXPECTED_DATE_DAY_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Hour, Is.EqualTo(EXPECTED_TIME_HOUR_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Minute, Is.EqualTo(EXPECTED_TIME_MINUTE_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Second, Is.EqualTo(EXPECTED_TIME_SECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.TimestampTypeValue.Millisecond, Is.EqualTo(EXPECTED_TIME_MILLISECOND_VALUE));
            Assert.That(response.StructureValue.TestStructure.AnyTypeValue.Payload, Is.EqualTo(EXPECTED_ANY_TYPE_VALUE.Payload.Span.ToArray()));
        }


        [Test, Ignore("Work in Progress...")]
        public void Should_Echo_Structure_In_UnobservableCommand()
        {
            // Arrange
            EchoStructureValue_Parameters parameters = new EchoStructureValue_Parameters()
            {
                StructureValue = new DataType_TestStructure()
                {
                    TestStructure = new DataType_TestStructure.Types.TestStructure_Struct
                    {
                        StringTypeValue = new SiLAFramework.String { Value = "SiLA2_Test_String_Value" },
                        IntegerTypeValue = new SiLAFramework.Integer { Value = 5124 },
                        RealTypeValue = new SiLAFramework.Real { Value = 3.1415926 },
                        BooleanTypeValue = new SiLAFramework.Boolean { Value = true },
                        BinaryTypeValue = new SiLAFramework.Binary { Value = ByteString.CopyFromUtf8("SiLA2_Binary_String_Value") },
                        DateTypeValue = new SiLAFramework.Date { Year = 2022, Month = 8, Day = 5 },
                        TimeTypeValue = new SiLAFramework.Time { Hour = 12, Minute = 34, Second = 56, Millisecond = 789 },
                        TimestampTypeValue = new SiLAFramework.Timestamp { Year = 2022, Month = 8, Day = 5, Hour = 12, Minute = 34, Second = 56, Millisecond = 789 },
                        AnyTypeValue = AnyType.CreateAnyTypeObject("SiLA2_Any_Type_String_Value")
                    }

                }
            };

            //string json = "{\r\n\t\"TestStructure\": {\r\n\t\t\"StringTypeValue\": {\r\n\t\t\t\"Value\": \"SiLA2_Test_String_Value\"\r\n\t\t},\r\n\t\t\"IntegerTypeValue\": {\r\n\t\t\t\"Value\": 5124\r\n\t\t},\r\n\t\t\"RealTypeValue\": {\r\n\t\t\t\"Value\": 3.1415926\r\n\t\t},\r\n\t\t\"BooleanTypeValue\": {\r\n\t\t\t\"Value\": true\r\n\t\t},\r\n\t\t\"BinaryTypeValue\": {\r\n\t\t\t\"Value\": \"U2lMQTJfQmluYXJ5X1N0cmluZ19WYWx1ZQ==\"\r\n\t\t},\r\n\t\t\"DateTypeValue\": {\r\n\t\t\t\"Day\": 5,\r\n\t\t\t\"Month\": 8,\r\n\t\t\t\"Year\": 2022,\r\n\t\t\t\"Timezone\": null\r\n\t\t},\r\n\t\t\"TimeTypeValue\": {\r\n\t\t\t\"Second\": 56,\r\n\t\t\t\"Minute\": 34,\r\n\t\t\t\"Hour\": 12,\r\n\t\t\t\"Timezone\": null,\r\n\t\t\t\"Millisecond\": 789\r\n\t\t},\r\n\t\t\"TimestampTypeValue\": {\r\n\t\t\t\"Second\": 56,\r\n\t\t\t\"Minute\": 34,\r\n\t\t\t\"Hour\": 12,\r\n\t\t\t\"Day\": 5,\r\n\t\t\t\"Month\": 8,\r\n\t\t\t\"Year\": 2022,\r\n\t\t\t\"Timezone\": null,\r\n\t\t\t\"Millisecond\": 789\r\n\t\t},\r\n\t\t\"AnyTypeValue\": {\r\n\t\t\t\"Type\": \"<?xml version=\\\"1.0\\\" encoding=\\\"utf-16\\\"?>\\r\\n<DataTypeType xmlns:xsi=\\\"http://www.w3.org/2001/XMLSchema-instance\\\" xmlns:xsd=\\\"http://www.w3.org/2001/XMLSchema\\\">\\r\\n <Basic xmlns=\\\"http://www.sila-standard.org\\\">String</Basic>\\r\\n</DataTypeType>\",\r\n\t\t\t\"Payload\": \"ChtTaUxBMl9BbnlfVHlwZV9TdHJpbmdfVmFsdWU=\"\r\n\t\t}\r\n\t}\r\n}";

            //var obj = JsonConvert.DeserializeObject(json);

            var payloadMap = new Dictionary<string, object>();

            var structure = new Structure
            {
                Identifier = "StructureValue",
                DisplayName = "Structure Value",
                Description = "The Structure value to be returned.",
                DataType = "TestStructure",
                Element = []
            };

            structure.Element.Add(new Sila2.Org.Silastandard.Protobuf.SiLAElement
            {
                Identifier = "StringTypeValue",
                DisplayName = "String Type Value",
                Description = "A value of SiLA data type String.",
                DataType = "Sila2.Org.Silastandard.Protobuf.String",
                Value = "SiLA2_Test_String_Value"
            });

            // System under Test
            
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            dynamic response = dynamicMessageService.ExecuteUnobservableCommand("EchoStructureValue", _channel, _silaStructureDataTypeFeature, payloadMap);

            // Assert
            Assert.Pass();
        }
    }
}
