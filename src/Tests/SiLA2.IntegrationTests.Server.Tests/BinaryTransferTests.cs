﻿using Moq;
using Sila2.Org.Silastandard.Test.Binarytransfertest.V1;
using System.Text;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    public class BinaryTransferTests
    {
        private GrpcChannel _channel;

        private SiLAFramework.BinaryUpload.BinaryUploadClient _binaryUploadClient;
        private SiLAFramework.BinaryDownload.BinaryDownloadClient _binaryDownloadClient;
        private BinaryClientService _binaryClientService;
        private Mock<ILogger<BinaryClientService>> _loggerMock;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();

            _binaryUploadClient = new SiLAFramework.BinaryUpload.BinaryUploadClient(_channel);
            _binaryDownloadClient = new SiLAFramework.BinaryDownload.BinaryDownloadClient(_channel);
            _loggerMock = new Mock<ILogger<BinaryClientService>>();
            _binaryClientService = new BinaryClientService(_binaryUploadClient, _binaryDownloadClient, _loggerMock.Object);
        }

        [Test]
        public async Task Deleted_BinaryUpload_Reference_Error()
        {
            // Arrange
            const string PARAMETER_IDENTIFIER = "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue";

            // System under Test
            var binaryTransferTestClient = new BinaryTransferTest.BinaryTransferTestClient(_channel);

            // Act
            var binaryTransferUUID = await _binaryClientService.UploadBinary(Encoding.UTF8.GetBytes("A_somewhat_longer_test_string_to_demonstrate_the_binary_upload"), 5, PARAMETER_IDENTIFIER);
            _binaryUploadClient.DeleteBinary(new SiLAFramework.DeleteBinaryRequest { BinaryTransferUUID = binaryTransferUUID });
            
            // Assert
            Assert.Catch(() => binaryTransferTestClient.EchoBinaryValue(new EchoBinaryValue_Parameters { BinaryValue = new SiLAFramework.Binary { BinaryTransferUUID = binaryTransferUUID } }));
        }
    }
}
