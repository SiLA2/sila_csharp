﻿using Grpc.Core;
using Sila2.Org.Silastandard;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    public class ObservableCommandTests
    {
        private GrpcChannel _channel;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
        }

        [Test]
        public async Task EchoValueAfterDelay_Works()
        {
            // Arrange
            const int SENT_VALUE = 123;
            const double EXECUTION_DELAY_IN_SECONDS = 5;

            // System under Test
            var observableCommandTestClient = new SiLAFramework.Test.Observablecommandtest.V1.ObservableCommandTest.ObservableCommandTestClient(_channel);

            // Act & Assert
            var startTime = DateTime.Now;

            // start command
            var cmdId = observableCommandTestClient.EchoValueAfterDelay(new SiLAFramework.Test.Observablecommandtest.V1.EchoValueAfterDelay_Parameters
            {
                Value = new Integer { Value = SENT_VALUE },
                Delay = new Real { Value = EXECUTION_DELAY_IN_SECONDS }
            }).CommandExecutionUUID;

            List<ExecutionInfo> receivedInfos = new List<ExecutionInfo>();

            // wait for command execution to finish
            try
            {
                using (var call = observableCommandTestClient.EchoValueAfterDelay_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        receivedInfos.Add(responseStream.Current);

                        // As long as the specified delay has not been passed, the command status must be waiting
                        Assert.That((DateTime.Now - startTime).TotalSeconds > EXECUTION_DELAY_IN_SECONDS || receivedInfos.Last().CommandStatus == ExecutionInfo.Types.CommandStatus.Waiting, "Command execution status must be [waiting] as long as the delay time has not been passed");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Assert.That((DateTime.Now - startTime).TotalSeconds >= EXECUTION_DELAY_IN_SECONDS, $"Execution time must be greater or equal to the specified delay time ({EXECUTION_DELAY_IN_SECONDS}s)");

            foreach(var ri in receivedInfos)
                Console.WriteLine($"--> STATUS: {ri.CommandStatus}   PROGRESS: {ri.ProgressInfo}");

            // at least 3 command execution info messages have to be sent
            Assert.That(receivedInfos.Count >= 3, "Three info messages are expected, status= [waiting] -> [running] -> [finishedSuccessfully]");

            // first command execution status has to be Waiting, second last has to be Running and last one has to be FinishedSuccessfully
            Assert.That(receivedInfos.First().CommandStatus == ExecutionInfo.Types.CommandStatus.Waiting, $"First received command execution status is [{receivedInfos.First().CommandStatus}] (must be [waiting])");
            Assert.That(receivedInfos.Reverse<ExecutionInfo>().Skip(1).Take(1).DefaultIfEmpty() != null);
            Assert.That(receivedInfos.Reverse<ExecutionInfo>().Skip(1).Take(1).First().CommandStatus == ExecutionInfo.Types.CommandStatus.Running, $"Second received command execution status is [{receivedInfos.Reverse<ExecutionInfo>().Skip(1).Take(1).First().CommandStatus}] (must be [running])");
            Assert.That(receivedInfos.Last().CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully, $"Last received command execution status is [{receivedInfos.Last().CommandStatus}] (must be [finishedSuccessfully]");

            // get response and check against sent value
            var response = observableCommandTestClient.EchoValueAfterDelay_Result(cmdId);
            Assert.That(response.ReceivedValue.Value, Is.EqualTo(SENT_VALUE), $"Received value {response.ReceivedValue.Value} differs from the sent value {SENT_VALUE}");
        }

        [Test, Ignore("To be implemnted...")]
        public void Should_Receive_IntermediateResponses()
        {
            // Arrange

            // System under Test

            // Act

            // Assert
            Assert.Pass();
        }
    }
}
