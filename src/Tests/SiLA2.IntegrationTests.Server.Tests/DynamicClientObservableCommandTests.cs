﻿using NUnit.Framework.Internal;
using Sila2.Org.Silastandard;
using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    internal class DynamicClientObservableCommandTests
    {
        private GrpcChannel _channel;
        private Feature _silaObservableCommandFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaObservableCommandFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "ObservableCommandTest-v1_0.sila.xml"));
        }

        [Test]
        public async Task Should_Call_ObservableCommand_With_Parameters()
        {
            // Arrange
            const long EXPECTED_ITERATIONS = 4;
            const string OPERATION_NAME = "Count";
            const string RESPONSE_PROPERTY = "IterationResponse";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "N", new SiLAFramework.Protobuf.Integer { Value = 5 } }, { "Delay", new SiLAFramework.Protobuf.Real { Value = 1.5 } } };

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, _channel, _silaObservableCommandFeature, payloadMap);

            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            List<ExecutionInfo> list = new();
            while (await asyncEnumerator.MoveNextAsync())
            {
                list.Add(asyncEnumerator.Current);
                if(asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully 
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }

            var result = dynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, _channel, _silaObservableCommandFeature, response.Item4);
            long iterations = ((SiLAFramework.Protobuf.Integer)(result.GetType().GetProperty(RESPONSE_PROPERTY).GetValue(result, null))).Value;

            // Assert
            Assert.That(list.Count, Is.GreaterThan(5));
            Assert.That(iterations, Is.EqualTo(EXPECTED_ITERATIONS));
        }

        [Test]
        public async Task Should_Receive_IntermediateResponeses()
        {
            // Arrange
            const long EXPECTED_ITERATIONS = 4;
            const long EXPECTED_INTERMEDIATE_COMMAND_RESULTS = 5;
            const string OPERATION_NAME = "Count";
            const string RESPONSE_PROPERTY = "IterationResponse";
            IDictionary<string, object> payloadMap = new Dictionary<string, object> { { "N", new SiLAFramework.Protobuf.Integer { Value = 5 } }, { "Delay", new SiLAFramework.Protobuf.Real { Value = 1.5 } } };
            List<object> intermediateResults = new List<object>();

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, _channel, _silaObservableCommandFeature, payloadMap);

            var asyncEnumeratorIntermediate = response.Item3.GetAsyncEnumerator();

#pragma warning disable CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.
            Task.Run(async () =>
            {
                try
                {
                    while (await asyncEnumeratorIntermediate.MoveNextAsync())
                    {
                        intermediateResults.Add(asyncEnumeratorIntermediate.Current);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            });
#pragma warning restore CS4014 // Da auf diesen Aufruf nicht gewartet wird, wird die Ausführung der aktuellen Methode vor Abschluss des Aufrufs fortgesetzt.

            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            List<ExecutionInfo> list = new();
            while (await asyncEnumerator.MoveNextAsync())
            {
                list.Add(asyncEnumerator.Current);
                if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }

            var result = dynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, _channel, _silaObservableCommandFeature, response.Item4);
            long iterations = ((SiLAFramework.Protobuf.Integer)(result.GetType().GetProperty(RESPONSE_PROPERTY).GetValue(result, null))).Value;

            // Assert
            Assert.That(list.Count, Is.GreaterThan(5));
            Assert.That(iterations, Is.EqualTo(EXPECTED_ITERATIONS));
            Assert.That(intermediateResults.Count, Is.EqualTo(EXPECTED_INTERMEDIATE_COMMAND_RESULTS));
            for (int i = 0; i < intermediateResults.Count; i++) 
            {
                dynamic item = intermediateResults[i];
                Assert.That(item.CurrentIteration.Value, Is.EqualTo(i));
            }
        }
    }
}
