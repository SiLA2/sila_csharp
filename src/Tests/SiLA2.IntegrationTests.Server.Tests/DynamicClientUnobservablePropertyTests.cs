﻿using SiLA2.Communication.Services;
using SiLA2.Server.Utils;
using UnitTest.Utils;

namespace SiLA2.IntegrationTests.Server.Tests
{
    [TestFixture]
    internal class DynamicClientUnobservablePropertyTests
    {
        private GrpcChannel _channel;
        private Feature _silaUnobservablePropertyFeature;

        [OneTimeSetUp]
        public async Task SetupOnce()
        {
            _channel = await ClientBootstrapper.SetupClient();
            _silaUnobservablePropertyFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("TestData", "UnobservablePropertyTest-v1_0.sila.xml"));
        }

        [Test]
        public void Should_Get_Unobservable_Property()
        {
            //Arrange
            const int EXPECTED_RESULT = 42;
            const string OPERATION_NAME = "AnswerToEverything";

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.GetUnobservableProperty(OPERATION_NAME, _channel, _silaUnobservablePropertyFeature);

            // Assert
            var propertyInfo = response.GetType().GetProperty(OPERATION_NAME).GetValue(response, null);
            var propertyValue = propertyInfo.GetType().GetProperty("Value").GetValue(propertyInfo, null);
            Assert.That(int.Parse(propertyValue.ToString()) == EXPECTED_RESULT);
        }

        [Test]
        public void Should_Get_Another_Unobservable_Property()
        {
            //Arrange
            const int EXPECTED_MIN_VALUE = 1685284992;
            var operationName = "SecondsSince1970";

            // System under Test
            var dynamicMessageService = new DynamicMessageService(new PayloadFactory());

            // Act
            var response = dynamicMessageService.GetUnobservableProperty(operationName, _channel, _silaUnobservablePropertyFeature);

            // Assert
            var propertyInfo = response.GetType().GetProperty(operationName).GetValue(response, null);
            var propertyValue = propertyInfo.GetType().GetProperty("Value").GetValue(propertyInfo, null);
            Assert.That(int.Parse(propertyValue.ToString()) > EXPECTED_MIN_VALUE);
        }
    }
}
