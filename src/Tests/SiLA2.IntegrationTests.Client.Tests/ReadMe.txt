﻿pip install --upgrade sila2-interop-communication-tester

Run something similar to this, after adjusting paths of server certificate and key...
-------------------------------------------------------------------------------------
py -m sila2_interop_communication_tester.test_server --cert-file "C:\Source\Code\SiLA2\sila_csharp\src\Tests\SiLA2.IntegrationTests.ServerApp\server.crt" --key-file "C:\Source\Code\SiLA2\sila_csharp\src\Tests\SiLA2.IntegrationTests.ServerApp\server.key" --html-file "C:\tmp\sila_integration_client_tests-resultat.html" --server-address 127.0.0.1:50052