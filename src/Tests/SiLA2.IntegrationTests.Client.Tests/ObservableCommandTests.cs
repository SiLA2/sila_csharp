﻿using Microsoft.Extensions.Configuration;
using Sila2.Org.Silastandard;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class ObservableCommandTests
    {
        const string FEATURE_FILE_NAME = "ObservableCommandTest-v1_0.sila.xml";
        private IDynamicConfigurator _client;
        private Feature _observableCommandFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, []);
            _observableCommandFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [Test]
        public async Task Should_Receive_Observable_Command_Result()
        {
            // Arrange
            const string OPERATION_NAME = "Count";
            const double DELAY = 1.0;
            const long COUNT = 7;
            const long EXPECTED_COUNT = 6;

            IDictionary<string, object> payloadMap = new Dictionary<string, object> {
                { "N", new Sila2.Org.Silastandard.Protobuf.Integer { Value = COUNT } },
                { "Delay", new Sila2.Org.Silastandard.Protobuf.Real { Value = DELAY } }
            };

            // System under Test & Act
            var response = _client.DynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, await _client.GetChannel(), _observableCommandFeature, payloadMap);

            var asyncEnumerator = response.Item2.GetAsyncEnumerator();

            while (await asyncEnumerator.MoveNextAsync())
            {
                if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
                    || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
                {
                    break;
                }
            }
            
            dynamic result = _client.DynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, await _client.GetChannel(), _observableCommandFeature, response.Item4);

            // Assert
            Assert.That(result.IterationResponse.Value, Is.EqualTo(EXPECTED_COUNT));
        }
    }
}