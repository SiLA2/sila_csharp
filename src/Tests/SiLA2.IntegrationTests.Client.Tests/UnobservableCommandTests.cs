using Microsoft.Extensions.Configuration;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class UnobservableCommandTests
    {
        const string FEATURE_FILE_NAME = "UnobservableCommandTest-v1_0.sila.xml";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _unobservableCommandFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _unobservableCommandFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [Test]
        public async Task Should_Call_Command_Without_Parameters_And_Responses()
        {
            // Arrange
            const string COMMAND_IDENTIFIER = "CommandWithoutParametersAndResponses";

            // System under Test & Act
            dynamic cmdWithoutParameterResponse = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, await _client.GetChannel(), _unobservableCommandFeature);

            // Assert
            Assert.That(cmdWithoutParameterResponse != null);
        }

        [Test]
        public async Task Should_Join_Integer_And_String()
        {
            // Arrange
            const string COMMAND_IDENTIFIER = "JoinIntegerAndString";
            const string EXPECTED_RESULT = "123abc";
            var payloadMap = new Dictionary<string, object> {
                        { "Integer", new Sila2.Org.Silastandard.Protobuf.Integer { Value = 123 } },
                        { "String", new Sila2.Org.Silastandard.Protobuf.String { Value = "abc" } }
                    };

            // System under Test & Act
            dynamic joinIntAndStringResponse = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, await _client.GetChannel(), _unobservableCommandFeature, payloadMap);

            // Assert
            Assert.That(joinIntAndStringResponse.JoinedParameters.Value, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public async Task Should_Convert_Integer_To_String()
        {
            // Arrange
            const string COMMAND_IDENTIFIER = "ConvertIntegerToString";
            const string EXPECTED_RESULT = "12345";
            var payloadMap = new Dictionary<string, object> { { "Integer", new Sila2.Org.Silastandard.Protobuf.Integer { Value = 12345 } } };

            // System under Test & Act
            dynamic intToStringResponse = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, await _client.GetChannel(), _unobservableCommandFeature, payloadMap);

            // Assert
            Assert.That(intToStringResponse.StringRepresentation.Value, Is.EqualTo(EXPECTED_RESULT));
        }

        [TestCase("", new string[] { "", "" })]
        [TestCase("a", new string[] { "a", "" })]
        [TestCase("ab", new string[] { "a", "b" })]
        [TestCase("abcde", new string[] { "a", "bcde" })]
        public async Task Should_Split_String_After_First_Character(string input, string[] output)
        {
            // Arrange
            const string COMMAND_IDENTIFIER = "SplitStringAfterFirstCharacter";
            var payloadMap = new Dictionary<string, object> { { "String", new Sila2.Org.Silastandard.Protobuf.String { Value = input } } };

            // System under Test & Act
            dynamic splitStringResponse = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, await _client.GetChannel(), _unobservableCommandFeature, payloadMap);

            // Assert
            Assert.That(splitStringResponse.FirstCharacter.Value, Is.EqualTo(output[0]));
            Assert.That(splitStringResponse.Remainder.Value, Is.EqualTo(output[1]));
        }
    }
}