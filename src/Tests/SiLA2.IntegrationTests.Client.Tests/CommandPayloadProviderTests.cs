﻿using System.Linq;

using Google.Protobuf.WellKnownTypes;

using Newtonsoft.Json;

using Sila2.Org.Silastandard.Protobuf;

using SiLA2.Communication.Services;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Client.Tests;

internal class CommandPayloadProviderTests
{
    const string FEATURE_FILE_NAME = "StructureDataTypeTest-v1_0.sila.xml";

    [Test, Ignore("Work in Progress...")]
    public void Should_Provide_CommandParameter_With_Values()
    {
        // Arrange
        Dictionary<Tuple<string, string, string>, string> parameterMap = [];
        Feature structureDataTypeTestFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        FeatureCommand featureCommand = structureDataTypeTestFeature.GetDefinedCommands().Single(x => x.Identifier == "EchoStructureValue");
        var featureParameter = featureCommand.Parameter.Single(x => x.Identifier.Equals("StructureValue"));

        string structureJson = File.ReadAllText(Path.Combine("TestData", "TestStructureTestData.json"));//JsonConvert.DeserializeObject(Path.Combine("TestData", "StructureDataTypeTest.json"));
        var structureObj = JsonConvert.DeserializeObject(structureJson);

        parameterMap[new Tuple<string, string, string>(structureDataTypeTestFeature.Identifier, featureCommand.Identifier, featureParameter.Identifier)] = structureJson;

        // System under Test
        var commandPayloadProvider = new CommandPayloadProvider();

        // Act
        var payloadMap = commandPayloadProvider.GetPayloadMap(parameterMap, featureCommand, structureDataTypeTestFeature);

        // Assert
        Assert.Pass();
    }

    
}
