﻿using Microsoft.Extensions.Configuration;
using SiLA2.Client.Dynamic;
using SiLA2.Server.Utils;
using SiLA2.Utils.Extensions;
using System.Text;

namespace SiLA2.IntegrationTests.Client.Tests
{
    [TestFixture]
    public class BinaryTransferTests
    {
        const string FEATURE_FILE_NAME = "BinaryTransferTest-v1_0.sila.xml";
        private string[] _args = { };
        private IDynamicConfigurator _client;
        private Feature _binaryTransferFeature;

        [OneTimeSetUp]
        public void Setup()
        {
            var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = configBuilder.Build();

            _client = new DynamicConfigurator(configuration, _args);
            _binaryTransferFeature = FeatureGenerator.ReadFeatureFromFile(Path.Combine("Features", FEATURE_FILE_NAME));
        }

        [Test]
        public async Task Should_Read_Small_Binary_Property()
        {
            // Arrange
            const string PROPERTY = "BinaryValueDirectly";
            const string EXPECTED_RESULT = "SiLA2_Test_String_Value";

            // System under Test & Act
            var response = _client.DynamicMessageService.GetUnobservableProperty(PROPERTY, await _client.GetChannel(), _binaryTransferFeature);

            // Assert
            byte[] responseBytes = (byte[])response.GetDynamicMember(PROPERTY).GetDynamicMember("Value");
            var responseString = Encoding.UTF8.GetString(responseBytes);
            
            Assert.That(responseString, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public async Task Should_Echo_Binary_Value()
        {
            // Arrange
            const string COMMAND = "EchoBinaryValue";
            const string RESPONSE_PROPERTY = "ReceivedValue";
            const string EXPECTED_RESULT = "abc";

            IDictionary<string, object> payloadMap = new Dictionary<string, object> {
                { "BinaryValue", new Sila2.Org.Silastandard.Protobuf.Binary { Value = Encoding.UTF8.GetBytes(EXPECTED_RESULT) } }
            };

            // System under Test & Act
            var response = _client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND, await _client.GetChannel(), _binaryTransferFeature, payloadMap);

            // Assert
            byte[] responseBytes = (byte[])response.GetDynamicMember(RESPONSE_PROPERTY).GetDynamicMember("Value");
            var responseString = Encoding.UTF8.GetString(responseBytes);

            Assert.That(responseString, Is.EqualTo(EXPECTED_RESULT));
        }
    }
}