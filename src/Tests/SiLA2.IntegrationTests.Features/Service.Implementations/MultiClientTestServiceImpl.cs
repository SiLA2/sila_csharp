﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Test.Multiclienttest.V1;
using SiLA2.Commands;
using SiLA2.Server;
using SiLA2.Server.Services;
using SiLA2.Server.Utils;

namespace SiLA2.IntegrationTests.Features.Service.Implementations
{
    public class MultiClientTestServiceImpl : MultiClientTest.MultiClientTestBase
    {
        private readonly ILogger<MultiClientTestServiceImpl> _logger;
        private readonly IObservableCommandManager<RunInParallel_Parameters, RunInParallel_Responses> _runInParallelCommandManager;
        private readonly IObservableCommandManager<RunQueued_Parameters, RunQueued_Responses> _runQueuedCommandManager;
        private readonly IObservableCommandManager<RejectParallelExecution_Parameters, RejectParallelExecution_Responses> _rejectParallelExecutionCommandManager;
        private readonly Feature _silaFeature;

        public MultiClientTestServiceImpl(ISiLA2Server siLA2Server, ILogger<MultiClientTestServiceImpl> logger,
                                          IObservableCommandManager<RunInParallel_Parameters, RunInParallel_Responses> runInParallelCommandManager,
                                          IObservableCommandManager<RunQueued_Parameters, RunQueued_Responses> runQueuedCommandManager,
                                          IObservableCommandManager<RejectParallelExecution_Parameters, RejectParallelExecution_Responses> rejectParallelExecutionCommandManager)
        {
            _logger = logger;
            _runInParallelCommandManager = runInParallelCommandManager;
            _runQueuedCommandManager = runQueuedCommandManager;
            _rejectParallelExecutionCommandManager = rejectParallelExecutionCommandManager;
            _silaFeature = siLA2Server.ReadFeature(Path.Combine("Features", "MultiClientTest-v1_0.sila.xml"));
        }

        public override async Task<CommandConfirmation> RunInParallel(RunInParallel_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "RunInParallel");

            var command = await _runInParallelCommandManager.AddCommand(request, (progress, request, token) =>
            {
                Task.Delay(Convert.ToInt32(request.Duration.Value * 1000)).Wait();
                return new RunInParallel_Responses();
            });
            return command.Confirmation;
        }

        public override async Task RunInParallel_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _runInParallelCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
        }

        public override Task<RunInParallel_Responses> RunInParallel_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _runInParallelCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        public override async Task<CommandConfirmation> RunQueued(RunQueued_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "RunQueued");
            var command = await _runQueuedCommandManager.AddCommand(request, (progress, request, token) =>
                        {
                            Task.Delay(Convert.ToInt32(request.Duration.Value * 1000)).Wait();
                            return new RunQueued_Responses();
                        }, TimeSpan.Zero, true);

            return command.Confirmation;
        }

        public override async Task RunQueued_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _runQueuedCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
        }

        public override Task<RunQueued_Responses> RunQueued_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _runQueuedCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }

        public override async Task<CommandConfirmation> RejectParallelExecution(RejectParallelExecution_Parameters request, ServerCallContext context)
        {
            request.ValidateCommandParameters(_silaFeature, "RejectParallelExecution");
            if(_rejectParallelExecutionCommandManager.IsCommandMapBusy)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.CommandExecutionNotAccepted, "Command is still running"));
                return null;
            }
            else
            {
                var command = await _rejectParallelExecutionCommandManager.AddCommand(request, (progress, request, token) =>
                {
                    Task.Delay(Convert.ToInt32(request.Duration.Value * 1000)).Wait();
                    return new RejectParallelExecution_Responses();
                });
                return command.Confirmation;
            }
        }

        public override async Task RejectParallelExecution_Info(CommandExecutionUUID cmdExecId, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _rejectParallelExecutionCommandManager.RegisterForInfo(cmdExecId, responseStream, context.CancellationToken);
            }
            catch (TaskCanceledException tcex)
            {
                _logger.LogWarning($"Request for CommandExecutionUUID {cmdExecId} terminated with TaskCancellation -> {tcex}");
            }
        }

        public override Task<RejectParallelExecution_Responses> RejectParallelExecution_Result(CommandExecutionUUID cmdExecId, ServerCallContext context)
        {
            var command = _rejectParallelExecutionCommandManager.GetCommand(cmdExecId);
            return Task.FromResult(command.Result);
        }
    }
}
