
## Introduction 
SilA2 Client implementation is hosted at https://gitlab.com/SiLA2/sila_csharp/-/tree/master/src/SiLA2.Client.Dynamic .
You´ll find an example implementation at https://gitlab.com/SiLA2/sila_csharp/-/blob/master/src/Tests/SiLA2.IntegrationTests.DynamicClientApp/ .

### Prerequisites
  * Linux / macOS
    * You´ll need the .NET 8 (or .NET 9) SDK >> https://dotnet.microsoft.com/download/dotnet/9.0
	* It´s not necessary to build applications with a GUI but if you do so an IDE like Visual Studio Code ( >> https://code.visualstudio.com/ ) would be convenient
  * Windows
    * Download free IDE Visual Studio 2022 Community ( >> https://visualstudio.microsoft.com/de/vs/community/ ), use commercial Visual Studio 2022 Version or Visual Studio Code as well
	* .NET 8 SDK and .NET 9 SDK are included in Visual Studio 2022...if you want to use Visual Studio Code or other IDEs you´ll have to download it on your own (see link above)

### Getting Started
  * Create Console Application
  * Search for & reference SiLA2.Client.Dynamic package at Nuget.org in Visual Studio or https://www.nuget.org/ 
  * Add appsettings.json to the project...something like the following...
```json
{
  "DetailedErrors": true,
  "Logging": {
    "LogLevel": {
      "Default": "Debug",
      "System": "Information",
      "Grpc": "Information",
      "Microsoft": "Information"
    }
  },
  "Connection": {
    "FQHN": "127.0.0.1",
    "Port": 50052,
    "ServerDiscovery": {
      "NIC": "Ethernet",
      "ServiceName": "_sila._tcp"
    }
  }
}
```
You might adjust FQHN (IP or CIDR or Fully Qualified Host Name), Port and NIC (Network Interface e.g. Ethernet, eth0, WLAN0 etc.). ServiceName ist part of mDNS message suffix used for server search in local network.

  * Load configuration and setup IoC Container
```cs
 var configBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
 var configuration = configBuilder.Build();

 var client = new DynamicConfigurator(configuration, args);
 ```
  * Search for SiLA2 servers
```cs
 _ = await client.GetFeatures();
 ```
 or add known servers
 ```cs
 _ = await client.GetFeatures(new[] { new ConnectionInfo("127.0.0.1", 50052) });
 ```
### Examples
An Example Server can be found at https://gitlab.com/SiLA2/sila_csharp/-/tree/master/src/Tests/SiLA2.IntegrationTests.ServerApp but you could use any other SiLA2 Server implementation for example found in sila_python or sila_java...
##### Call Unobservable Property
```cs
 private static void CallUnobservableProperty(IDynamicConfigurator client, GrpcChannel channel)
 {
     Console.WriteLine($"{Environment.NewLine}Calling unobservable property to get Server UUID...");
     var silaServiceFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/core/SiLAService/v1");
     dynamic serverUUIDResponse = client.DynamicMessageService.GetUnobservableProperty("ServerUUID", channel, silaServiceFeature);
     Console.WriteLine($"Response => ServerUUID: {serverUUIDResponse.ServerUUID.Value}");
 }
 ```
##### Call Unobservable Command
```cs
private static void CallUnobservableCommand(IDynamicConfigurator client, GrpcChannel channel)
{
    const string COMMAND_IDENTIFIER = "JoinIntegerAndString";
    Console.WriteLine($"{Environment.NewLine}Calling Unobservable Command '{COMMAND_IDENTIFIER}' with Integer and String Parameter...");

    int intVal = int.MinValue;
    do
    {
        Console.WriteLine("Enter Integer");
        string tmp = Console.ReadLine();
        if (int.TryParse(tmp, out int intValTmp))
        {
            intVal = intValTmp;
            break;
        }
    } while (true);
    Console.WriteLine("Enter String");
    string strVal = Console.ReadLine();

    var unobservableCommandFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/UnobservableCommandTest/v1");

    Dictionary<string, object> payloadMap = new Dictionary<string, object> {
            { "Integer", new Sila2.Org.Silastandard.Protobuf.Integer { Value = intVal } },
            { "String", new Sila2.Org.Silastandard.Protobuf.String { Value = strVal } }
        };

    dynamic joinIntAndStringResponse = client.DynamicMessageService.ExecuteUnobservableCommand(COMMAND_IDENTIFIER, channel, unobservableCommandFeature, payloadMap);
    Console.WriteLine($"Response => JoinedParameters: {joinIntAndStringResponse.JoinedParameters.Value}");
}
 ```
##### Call Observable Property
```cs
private async static Task CallObservableProperty(IDynamicConfigurator client, GrpcChannel channel)
{
    const string PROPERTY_NAME = "Alternating";
    Console.WriteLine($"{Environment.NewLine}Calling Observable Property returning alternating Boolean Value for 7 seconds...");
    var silaObservablePropertyFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/ObservablePropertyTest/v1");
    var response = client.DynamicMessageService.SubcribeObservableProperty(PROPERTY_NAME, channel, silaObservablePropertyFeature);

    var asyncEnumerator = response.GetAsyncEnumerator();

    int i = 0;
    while (await asyncEnumerator.MoveNextAsync() && i <= 7)
    {
        dynamic responseResultPropertyValue = asyncEnumerator.Current;
        Console.WriteLine($"Response => {responseResultPropertyValue.Alternating.Value}");
        i++;
    }
}
 ```
##### Call Observable Command
```cs
private static async Task CallObservableCommand(DynamicConfigurator client, GrpcChannel channel)
{
    const string OPERATION_NAME = "Count";
    const double DELAY = 1.0;
    Console.WriteLine($"{Environment.NewLine}Calling Observable Command for 7 seconds with delay of 1 second...");

    var observableCommandFeature = client.ServerFeatureMap[_connectionInfo].Values.Single(x => x.FullyQualifiedIdentifier == "org.silastandard/test/ObservableCommandTest/v1");

    IDictionary<string, object> payloadMap = new Dictionary<string, object> {
        { "N", new Sila2.Org.Silastandard.Protobuf.Integer { Value = 7 } },
        { "Delay", new Sila2.Org.Silastandard.Protobuf.Real { Value = DELAY } }
    };

    var response = client.DynamicMessageService.ExecuteObservableCommand(OPERATION_NAME, channel, observableCommandFeature, payloadMap);

    var asyncEnumerator = response.Item2.GetAsyncEnumerator();

    while (await asyncEnumerator.MoveNextAsync())
    {
        Console.WriteLine($"Response => {asyncEnumerator.Current.CommandStatus}");
        if (asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully
            || asyncEnumerator.Current.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError)
        {
            break;
        }
    }

    dynamic result = client.DynamicMessageService.GetObservableCommandResult(response.Item1.CommandExecutionUUID, OPERATION_NAME, channel, observableCommandFeature, response.Item3);
    Console.WriteLine($"Result Response => Number of Iterations => {result.IterationResponse.Value}");
}
 ```
  

