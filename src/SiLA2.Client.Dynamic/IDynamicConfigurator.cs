﻿using SiLA2.Communication.Services;
using SiLA2.Network.Discovery.mDNS;

namespace SiLA2.Client.Dynamic
{
    public interface IDynamicConfigurator : IConfigurator
    {
        IDictionary<ConnectionInfo, IDictionary<string, Feature>> ServerFeatureMap { get; }

        IDynamicMessageService DynamicMessageService { get; }

        Task<IDictionary<ConnectionInfo, IDictionary<string, Feature>>> GetFeatures(IEnumerable<ConnectionInfo> connections = null);
    }
}