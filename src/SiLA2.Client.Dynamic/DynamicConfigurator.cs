﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Sila2.Org.Silastandard.Core.Silaservice.V1;

using SiLA2.Communication.Services;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Server.Utils;

namespace SiLA2.Client.Dynamic
{
    public class DynamicConfigurator : Configurator, IDynamicConfigurator
    {
        private readonly ILogger<DynamicConfigurator> _logger;

        public IDictionary<ConnectionInfo, IDictionary<string, Feature>> ServerFeatureMap { get; } = new Dictionary<ConnectionInfo, IDictionary<string, Feature>>();

        public IDynamicMessageService DynamicMessageService => ServiceProvider.GetService<IDynamicMessageService>();

        public DynamicConfigurator(IConfiguration configuration, string[] args) : base(configuration, args)
        {
            Container.AddSingleton<IPayloadFactory, PayloadFactory>();
            Container.AddSingleton<IDynamicMessageService, DynamicMessageService>();
            UpdateServiceProvider();
            _logger = ServiceProvider.GetRequiredService<ILogger<DynamicConfigurator>>();
        }

        public async Task<IDictionary<ConnectionInfo, IDictionary<string, Feature>>> GetFeatures(IEnumerable<ConnectionInfo> connections = null)
        {
            if(connections == null)
            {
                _ = await SearchForServers();
            }
            else
            {
                foreach (var server in connections)
                {
                    var uuid = Guid.Parse(server.ServerUuid);
                    DiscoveredServers[uuid] = server;
                    _logger.LogInformation($"Found Server at '{server}' with name '{server.ServerName}'");
                }
            }

            ServerFeatureMap.Clear();
            
            foreach(var connectionInfo in DiscoveredServers.Values)
            {
                var channel = await GetChannel(connectionInfo.Address, connectionInfo.Port);
                ServerFeatureMap.Add(connectionInfo, new Dictionary<string, Feature>());
                    
                var serviceClient = new SiLAService.SiLAServiceClient(channel);
                var implementedFeatures = await serviceClient.Get_ImplementedFeaturesAsync(new Get_ImplementedFeatures_Parameters());
                    
                foreach(var featureId in implementedFeatures.ImplementedFeatures)
                {
                    var featureDefinitionResponse = serviceClient.GetFeatureDefinition(new GetFeatureDefinition_Parameters { FeatureIdentifier = featureId });
                    var feature = FeatureGenerator.ReadFeatureFromXml(featureDefinitionResponse.FeatureDefinition.Value);
                    ServerFeatureMap[connectionInfo].Add(featureId.Value, feature);
                }
            }   

            return ServerFeatureMap;
        }
    }
}