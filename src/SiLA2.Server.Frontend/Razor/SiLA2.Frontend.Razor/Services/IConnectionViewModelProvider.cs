﻿using SiLA2.Frontend.Razor.Model;
using System.Collections.Concurrent;

namespace SiLA2.Frontend.Razor.Services
{
    public interface IConnectionViewModelProvider
    {
        ConcurrentDictionary<string, ConnectionViewModel> Servers { get; }
    }
}