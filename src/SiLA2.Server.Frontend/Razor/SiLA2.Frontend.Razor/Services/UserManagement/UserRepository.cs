﻿using SiLA2.Database.SQL;
using SiLA2.Frontend.Razor.Services.UserManagement.Domain;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(IDbUserContext context) : base(context) { }
    }
}