﻿using SiLA2.Frontend.Razor.Services.UserManagement.Domain;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services.UserManagement
{
    public interface IUserService
    {
        Task CreateUser(string login, string pwd, Role role = Role.Standard);
        IQueryable<User> GetUsers();
        Task<User> GetUser(int id);
        User GetUser(string login);
        Task InsertUser(User user);
        Task UpdateUser(User user, bool hashPwd = false);
        Task DeleteUser(User user);
    }
}