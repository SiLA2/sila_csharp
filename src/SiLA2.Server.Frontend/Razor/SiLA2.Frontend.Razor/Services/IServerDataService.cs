﻿using SiLA2.Server;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SiLA2.Frontend.Razor.Services
{
    public interface IServerDataService
    {
        Task<ServerData> GetServerData(string host, int port, bool acceptAnyServerCertificated = true);
        Task<IDictionary<string, Feature>> GetServerFeatures(string host, int port, bool acceptAnyServerCertificated = true);
    }
}