﻿using Microsoft.AspNetCore.Components.Forms;
using SiLA2.Server.Utils;
using System.Collections.Generic;
using Constants = SiLA2.Utils.Security.Constants;

namespace SiLA2.Frontend.Razor.ViewModels
{
    internal class SecurityViewModel
    {
        const string RESSOURCE_NAME_FILETRANSFER_FEATURE = "SiLA2.Frontend.Razor.Features.FileTransfer-v1_0.sila.xml";

        public IEnumerable<string> CertificateItemNames => [Constants.SERVER_CRT_FILENAME, Constants.SERVER_KEY_FILENAME, Constants.CA_CRT_FILENAME, Constants.CA_KEY_FILENAME];
        public string SelectedCertificateItemName { get; set; }
        public IDictionary<string, IBrowserFile> CertificateItemMap { get; set; } = new Dictionary<string, IBrowserFile>() { { Constants.SERVER_CRT_FILENAME, null }, { Constants.SERVER_KEY_FILENAME, null }, { Constants.CA_CRT_FILENAME, null }, { Constants.CA_KEY_FILENAME, null } };
        public Feature FileTransferFeature { get; }

        public SecurityViewModel()
        {
            SelectedCertificateItemName = Constants.CA_CRT_FILENAME;
            var stream = GetType().Assembly.GetManifestResourceStream(RESSOURCE_NAME_FILETRANSFER_FEATURE);
            FileTransferFeature = FeatureGenerator.ReadFeatureFromStream(stream);
        }
    }
}
