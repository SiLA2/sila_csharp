﻿using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiLA2.Network.Discovery.mDNS;
using SiLA2.Utils.CmdArgs.Client;
using SiLA2.Utils.Config;
using SiLA2.Utils.Extensions;
using SiLA2.Utils.gRPC;
using SiLA2.Utils.Network;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class Configurator : IConfigurator
    {
        private readonly ILogger<Configurator> _logger;
        private IClientConfig _clientConfig;

        public IServiceCollection Container { get; } = new ServiceCollection();
        public IServiceProvider ServiceProvider { get; private set; }
        public IDictionary<Guid, ConnectionInfo> DiscoveredServers { get; } = new Dictionary<Guid, ConnectionInfo>();

        public Configurator(IConfiguration configuration, string[] args)
        {
            SetupContainer(configuration);
            ServiceProvider = Container.BuildServiceProvider();
            args.ParseClientCmdLineArgs<CmdLineClientArgs>(_clientConfig);
            _logger = ServiceProvider.GetRequiredService<ILogger<Configurator>>();
        }

        public async Task<IDictionary<Guid, ConnectionInfo>> SearchForServers()
        {
            try
            {
                var serviceFinder = ServiceProvider.GetRequiredService<IServiceFinder>();
                var servers = await serviceFinder.GetConnections(_clientConfig.DiscoveryServiceName, _clientConfig.NetworkInterface, 3000);

                foreach (var server in servers)
                {
                    var uuid = Guid.Parse(server.ServerUuid);
                    DiscoveredServers[uuid] = server;
                    _logger.LogInformation($"Found Server at '{server}' with name '{server.ServerName}'");
                }

                return DiscoveredServers;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while searching for servers.");
                throw;
            }
        }

        private void SetupContainer(IConfiguration configuration)
        {
            Container.AddLogging();
            Container.AddSingleton<IServiceFinder, ServiceFinder>();
            Container.AddSingleton<INetworkService, NetworkService>();
            Container.AddSingleton<IGrpcChannelProvider, GrpcChannelProvider>();
            _clientConfig = new ClientConfig(configuration);
            Container.AddSingleton(_clientConfig);
        }

        public async Task<GrpcChannel> GetChannel(bool acceptAnyServerCertificate = true)
        {
            try
            {
                var grpcChannelProvider = ServiceProvider.GetRequiredService<IGrpcChannelProvider>();
                return await grpcChannelProvider.GetChannel(_clientConfig.IpOrCdirOrFullyQualifiedHostName, _clientConfig.Port, acceptAnyServerCertificate);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting gRPC channel.");
                throw;
            }
        }

        public async Task<GrpcChannel> GetChannel(string host, int port, bool acceptAnyServerCertificate = true, X509Certificate2 ca = null)
        {
            try
            {
                var grpcChannelProvider = ServiceProvider.GetRequiredService<IGrpcChannelProvider>();
                return await grpcChannelProvider.GetChannel(host, port, acceptAnyServerCertificate, ca);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while getting gRPC channel.");
                throw;
            }
        }

        public void UpdateServiceProvider()
        {
            ServiceProvider = Container.BuildServiceProvider();
        }
    }
}
