﻿using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Sila2.Org.Silastandard;
using SiLA2.Server.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SiLA2.Client
{
    public class BinaryClientService : IBinaryClientService
    {
        private readonly BinaryUpload.BinaryUploadClient _binaryUploadClient;
        private readonly BinaryDownload.BinaryDownloadClient _binaryDownloadClient;
        private readonly ILogger<BinaryClientService> _logger;

        public BinaryClientService(BinaryUpload.BinaryUploadClient binaryUploadClient, BinaryDownload.BinaryDownloadClient binaryDownloadClient, ILogger<BinaryClientService> logger)
        {
            _logger = logger;
            _binaryUploadClient = binaryUploadClient;
            _binaryDownloadClient = binaryDownloadClient;
        }

        public async Task<string> UploadBinary(byte[] value, int chunkSize, string parameterIdentifier)
        {
            try
            {
                var binaryTransferUUID = await CreateBinaryAsync(value.Length, chunkSize, parameterIdentifier);

                using (var call = _binaryUploadClient.UploadChunk())
                {
                    await UploadChunksAsync(call, value, chunkSize, binaryTransferUUID);
                    await call.RequestStream.CompleteAsync();
                }

                _logger.LogInformation($"{value.Length} bytes uploaded with {value.Length / chunkSize + (value.Length % chunkSize != 0 ? 1 : 0)} chunks");

                return binaryTransferUUID;
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorHandling.HandleException(ex));
                throw;
            }
        }

        private async Task<string> CreateBinaryAsync(int length, int chunkSize, string parameterIdentifier)
        {
            var response = await _binaryUploadClient.CreateBinaryAsync(new CreateBinaryRequest
            {
                BinarySize = (ulong)length,
                ChunkCount = (uint)(length / chunkSize + (length % chunkSize != 0 ? 1 : 0)),
                ParameterIdentifier = parameterIdentifier
            });

            return response.BinaryTransferUUID;
        }

        private async Task UploadChunksAsync(AsyncDuplexStreamingCall<UploadChunkRequest, UploadChunkResponse> call, byte[] value, int chunkSize, string binaryTransferUUID)
        {
            var chunkIndex = 0;
            var offset = 0;
            while (offset < value.Length)
            {
                var sentChunkSize = Math.Min(chunkSize, value.Length - offset);

                await call.RequestStream.WriteAsync(new UploadChunkRequest
                {
                    BinaryTransferUUID = binaryTransferUUID,
                    ChunkIndex = (uint)chunkIndex,
                    Payload = ByteString.CopyFrom(value, offset, sentChunkSize)
                });

                await call.ResponseStream.MoveNext();
                ValidateChunkResponse(call.ResponseStream.Current, binaryTransferUUID, chunkIndex);

                offset += sentChunkSize;
                chunkIndex++;
            }
        }

        private void ValidateChunkResponse(UploadChunkResponse response, string expectedUUID, int expectedIndex)
        {
            if (response.BinaryTransferUUID != expectedUUID)
            {
                throw new Exception($"Exception while uploading chunk: received binary transfer UUID '{response.BinaryTransferUUID}' differs from the sent one '{expectedUUID}'");
            }

            if (response.ChunkIndex != expectedIndex)
            {
                throw new Exception($"Exception while uploading chunk: received chunk index {response.ChunkIndex} differs from the sent one ({expectedIndex})");
            }
        }

        public async Task<byte[]> DownloadBinary(string binaryTransferUuid, int chunkSize)
        {
            var binaryInfo = await _binaryDownloadClient.GetBinaryInfoAsync(new GetBinaryInfoRequest { BinaryTransferUUID = binaryTransferUuid });
            byte[] buffer = new byte[binaryInfo.BinarySize];

            using (var call = _binaryDownloadClient.GetChunk())
            {
                var responseReaderTask = ReadChunksAsync(call, buffer);

                await RequestChunksAsync(call, binaryTransferUuid, chunkSize, binaryInfo.BinarySize);

                await call.RequestStream.CompleteAsync();
                await responseReaderTask;
            }

            return buffer;
        }

        private async Task ReadChunksAsync(AsyncDuplexStreamingCall<GetChunkRequest, GetChunkResponse> call, byte[] buffer)
        {
            while (await call.ResponseStream.MoveNext())
            {
                try
                {
                    var payload = call.ResponseStream.Current.Payload.ToArray();
                    payload.CopyTo(buffer, (int)call.ResponseStream.Current.Offset);
                }
                catch (Exception e)
                {
                    _logger.LogError("Binary download failed: " + e.Message);
                }
            }
        }

        private async Task RequestChunksAsync(AsyncDuplexStreamingCall<GetChunkRequest, GetChunkResponse> call, string binaryTransferUuid, int chunkSize, ulong binarySize)
        {
            ulong currentIndex = 0;
            while (currentIndex < binarySize)
            {
                var length = Math.Min(chunkSize, (uint)(binarySize - currentIndex));
                await call.RequestStream.WriteAsync(new GetChunkRequest
                {
                    BinaryTransferUUID = binaryTransferUuid,
                    Offset = currentIndex,
                    Length = Convert.ToUInt32(length)
                });
                currentIndex += Convert.ToUInt64(length);
            }
        }
    }
}
