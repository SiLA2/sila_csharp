﻿using NetMQ;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SiLA2.IPC.NetMQ
{
    public interface IZeroMqServer
    {
        Task Run(CancellationToken cancellationToken = default);
        void SetRequestProcessing(Func<NetMQMessage, string> requestProcessing);
    }
}