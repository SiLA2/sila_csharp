﻿# Create Dashboard 

Run Aspire Standalone OpenTelemetry Dashboard by

```shell
docker run --rm -it -p 18888:18888 -p 4317:18889 -d --name aspire-dashboard mcr.microsoft.com/dotnet/aspire-dashboard:latest
```

in your shell to create container in docker (Linux) or Docker Desktop (Window & macOS).