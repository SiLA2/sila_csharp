using Projects;

var builder = DistributedApplication.CreateBuilder(args);

builder.AddProject<SiLA2_IntegrationTests_ServerApp>("sila2-integrationtest-app");
builder.AddProject<SiLA2_Temperature_Server_App>("sila2-temperature-server-example-app");

builder.Build().Run();
